.syntax unified
.cpu cortex-m4

.global arm_vector_table
.global arm_vector_reset

.section .text.arm_vector_reset
.type arm_vector_reset, %function
@ Copy valyes from data_begin to RAM. made for an M0 but M4 could optimise this with an increment on load
arm_vector_reset:
	ldr r1, =_data_begin
	ldr r3, =_data_end
	cmp r1, r3
	beq data_end
	ldr r0, =_data_flash
data_loop:
	ldr r2, [r0]
	adds r0, #4
	str r2, [r1]
	adds r1, #4
	cmp r1, r3
	bne data_loop
data_end:

	ldr r0, =_bss_begin
	ldr r1, =_bss_end
	cmp r0, r1
	beq bss_end
	movs r2, #0
bss_loop:
	str r2, [r0]
	adds r0, #4
	cmp r0, r1
	bne bss_loop
bss_end:

	ldr r0, =start_the_game_already
	blx r0
.size arm_vector_reset, .-arm_vector_reset

.section .arm_vector_table,"a",%progbits
.type arm_vector_table, %object
arm_vector_table:
	.word _stack_end
	.word arm_vector_reset

	.word arm_vector_nmi
	.word arm_vector_hard_fault
	.word arm_vector_mem_manage
	.word arm_vector_bus_fault
	.word arm_vector_usage_fault
	.word 0
	.word 0
	.word 0
	.word 0
	.word arm_vector_svc
	.word 0 @ arm_vector_monitor
	.word 0
	.word arm_vector_pend_sv
	.word arm_vector_system_tick

	.word arm_vector_irq_wwdg
	.word arm_vector_irq_pvd_pvm
	.word arm_vector_irq_rtc_tamp_css_lse
	.word arm_vector_irq_rtc_wkup
	.word arm_vector_irq_flash
	.word arm_vector_irq_rcc
	.word arm_vector_irq_exti_0
	.word arm_vector_irq_exti_1
	.word arm_vector_irq_exti_2
	.word arm_vector_irq_exti_3
	.word arm_vector_irq_exti_4
	.word arm_vector_irq_dma1_ch1
	.word arm_vector_irq_dma1_ch2
	.word arm_vector_irq_dma1_ch3
	.word arm_vector_irq_dma1_ch4
	.word arm_vector_irq_dma1_ch5
	.word arm_vector_irq_dma1_ch6
	.word arm_vector_irq_dma1_ch7
	.word arm_vector_irq_adc_1_2
	.word arm_vector_irq_usb_hp
	.word arm_vector_irq_usb_lp
	.word arm_vector_irq_fdcan1_it0
	.word arm_vector_irq_fdcan1_it1
	.word arm_vector_irq_exti9_5
	.word arm_vector_irq_tim1_brk_tim15
	.word arm_vector_irq_tim1_up_tim_16
	.word arm_vector_irq_tim1_trg_com_tim17_tim1_dir_tim1_idx
	.word arm_vector_irq_tim1_cc
	.word arm_vector_irq_tim2
	.word arm_vector_irq_tim3
	.word arm_vector_irq_tim4
	.word arm_vector_irq_i2c1_ev
	.word arm_vector_irq_i2c1_er
	.word arm_vector_irq_i2c2_ev
	.word arm_vector_irq_i2c2_er
	.word arm_vector_irq_spi1
	.word arm_vector_irq_spi2
	.word arm_vector_irq_usart1
	.word arm_vector_irq_usart2
	.word arm_vector_irq_usart3
	.word arm_vector_irq_exti15_10
	.word arm_vector_irq_rtc_alarm
	.word arm_vector_irq_usb_wake_up
	.word arm_vector_irq_tim8_brk_tim8_terr_tim8_ierr
	.word arm_vector_irq_tim8_up
	.word arm_vector_irq_tim8_trg_com_tim8_dir_tim8_idx
	.word arm_vector_irq_tim1_cc
	.word arm_vector_irq_adc3
	.word arm_vector_irq_fsmc
	.word arm_vector_irq_lptim1
	.word arm_vector_irq_tim5
	.word arm_vector_irq_spi3
	.word arm_vector_irq_uart4
	.word arm_vector_irq_uart5
	.word arm_vector_irq_tim6_dacunder
	.word arm_vector_irq_tim7_dacunder
	.word arm_vector_irq_dma2_ch1
	.word arm_vector_irq_dma2_ch2
	.word arm_vector_irq_dma2_ch3
	.word arm_vector_irq_dma2_ch4
	.word arm_vector_irq_dma2_ch5
	.word arm_vector_irq_adc4
	.word arm_vector_irq_adc5
	.word arm_vector_irq_ucpd1_exti43
	.word arm_vector_irq_comp1_2_3
	.word arm_vector_irq_comp4_5_6
	.word arm_vector_irq_comp7
	.word arm_vector_irq_hrtim_master_irqn
	.word arm_vector_irq_hrtim_tima_irqn
	.word arm_vector_irq_hrtim_timb_irqn
	.word arm_vector_irq_hrtim_timc_irqn
	.word arm_vector_irq_hrtim_timd_irqn
	.word arm_vector_irq_hrtim_time_irqn
	.word arm_vector_irq_hrtim_tim_flt_irqn
	.word arm_vector_irq_hrtim_timf_irqn
	.word arm_vector_irq_crs
	.word arm_vector_irq_sai
	.word arm_vector_irq_tim20_brk_tim20_terr_tim20_ierr
	.word arm_vector_irq_tim20_up
	.word arm_vector_irq_tim20_trg_com_tim20_dir_tim20_idx
	.word arm_vector_irq_tim20_cc
	.word arm_vector_irq_fpu
	.word arm_vector_irq_i2c4_ev
	.word arm_vector_irq_i2c4_er
	.word arm_vector_irq_spi4
	.word arm_vector_irq_aes
	.word arm_vector_irq_fdcan2_it0
	.word arm_vector_irq_fdcan2_it1
	.word arm_vector_irq_fdcan3_it0
	.word arm_vector_irq_fdcan3_it1
	.word arm_vector_irq_rng
	.word arm_vector_irq_lpuart
	.word arm_vector_irq_i2c3_ev
	.word arm_vector_irq_i2c3_er
	.word arm_vector_irq_dmamux_ovr
	.word arm_vector_irq_quadspi
	.word arm_vector_irq_dma1_ch8
	.word arm_vector_irq_dma2_ch6
	.word arm_vector_irq_dma2_ch7
	.word arm_vector_irq_dma2_ch8
	.word arm_vector_irq_cordic
	.word arm_vector_irq_fmac

.size arm_vector_table, .-arm_vector_table

.weak arm_vector_nmi
.weak arm_vector_hard_fault
.weak arm_vector_mem_manage
.weak arm_vector_bus_fault
.weak arm_vector_usage_fault
.weak arm_vector_svc
.weak arm_vector_pend_sv
.weak arm_vector_system_tick

.weak arm_vector_irq_wwdg
.weak arm_vector_irq_pvd_pvm
.weak arm_vector_irq_rtc_tamp_css_lse
.weak arm_vector_irq_rtc_wkup
.weak arm_vector_irq_flash
.weak arm_vector_irq_rcc
.weak arm_vector_irq_exti_0
.weak arm_vector_irq_exti_1
.weak arm_vector_irq_exti_2
.weak arm_vector_irq_exti_3
.weak arm_vector_irq_exti_4
.weak arm_vector_irq_dma1_ch1
.weak arm_vector_irq_dma1_ch2
.weak arm_vector_irq_dma1_ch3
.weak arm_vector_irq_dma1_ch4
.weak arm_vector_irq_dma1_ch5
.weak arm_vector_irq_dma1_ch6
.weak arm_vector_irq_dma1_ch7
.weak arm_vector_irq_adc_1_2
.weak arm_vector_irq_usb_hp
.weak arm_vector_irq_usb_lp
.weak arm_vector_irq_fdcan1_it0
.weak arm_vector_irq_fdcan1_it1
.weak arm_vector_irq_exti9_5
.weak arm_vector_irq_tim1_brk_tim15
.weak arm_vector_irq_tim1_up_tim_16
.weak arm_vector_irq_tim1_trg_com_tim17_tim1_dir_tim1_idx
.weak arm_vector_irq_tim1_cc
.weak arm_vector_irq_tim2
.weak arm_vector_irq_tim3
.weak arm_vector_irq_tim4
.weak arm_vector_irq_i2c1_ev
.weak arm_vector_irq_i2c1_er
.weak arm_vector_irq_i2c2_ev
.weak arm_vector_irq_i2c2_er
.weak arm_vector_irq_spi1
.weak arm_vector_irq_spi2
.weak arm_vector_irq_usart1
.weak arm_vector_irq_usart2
.weak arm_vector_irq_usart3
.weak arm_vector_irq_exti15_10
.weak arm_vector_irq_rtc_alarm
.weak arm_vector_irq_usb_wake_up
.weak arm_vector_irq_tim8_brk_tim8_terr_tim8_ierr
.weak arm_vector_irq_tim8_up
.weak arm_vector_irq_tim8_trg_com_tim8_dir_tim8_idx
.weak arm_vector_irq_tim1_cc
.weak arm_vector_irq_adc3
.weak arm_vector_irq_fsmc
.weak arm_vector_irq_lptim1
.weak arm_vector_irq_tim5
.weak arm_vector_irq_spi3
.weak arm_vector_irq_uart4
.weak arm_vector_irq_uart5
.weak arm_vector_irq_tim6_dacunder
.weak arm_vector_irq_tim7_dacunder
.weak arm_vector_irq_dma2_ch1
.weak arm_vector_irq_dma2_ch2
.weak arm_vector_irq_dma2_ch3
.weak arm_vector_irq_dma2_ch4
.weak arm_vector_irq_dma2_ch5
.weak arm_vector_irq_adc4
.weak arm_vector_irq_adc5
.weak arm_vector_irq_ucpd1_exti43
.weak arm_vector_irq_comp1_2_3
.weak arm_vector_irq_comp4_5_6
.weak arm_vector_irq_comp7
.weak arm_vector_irq_hrtim_master_irqn
.weak arm_vector_irq_hrtim_tima_irqn
.weak arm_vector_irq_hrtim_timb_irqn
.weak arm_vector_irq_hrtim_timc_irqn
.weak arm_vector_irq_hrtim_timd_irqn
.weak arm_vector_irq_hrtim_time_irqn
.weak arm_vector_irq_hrtim_tim_flt_irqn
.weak arm_vector_irq_hrtim_timf_irqn
.weak arm_vector_irq_crs
.weak arm_vector_irq_sai
.weak arm_vector_irq_tim20_brk_tim20_terr_tim20_ierr
.weak arm_vector_irq_tim20_up
.weak arm_vector_irq_tim20_trg_com_tim20_dir_tim20_idx
.weak arm_vector_irq_tim20_cc
.weak arm_vector_irq_fpu
.weak arm_vector_irq_i2c4_ev
.weak arm_vector_irq_i2c4_er
.weak arm_vector_irq_spi4
.weak arm_vector_irq_aes
.weak arm_vector_irq_fdcan2_it0
.weak arm_vector_irq_fdcan2_it1
.weak arm_vector_irq_fdcan3_it0
.weak arm_vector_irq_fdcan3_it1
.weak arm_vector_irq_rng
.weak arm_vector_irq_lpuart
.weak arm_vector_irq_i2c3_ev
.weak arm_vector_irq_i2c3_er
.weak arm_vector_irq_dmamux_ovr
.weak arm_vector_irq_quadspi
.weak arm_vector_irq_dma1_ch8
.weak arm_vector_irq_dma2_ch6
.weak arm_vector_irq_dma2_ch7
.weak arm_vector_irq_dma2_ch8
.weak arm_vector_irq_cordic
.weak arm_vector_irq_fmac