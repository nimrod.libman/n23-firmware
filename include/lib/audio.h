#ifndef N23_AUDIO_H
#define N23_AUDIO_H

#include <stdint.h>

// Define N23_AUDIO_8_BIT before including this header to use 8 bit audio.
// Will use 16 bit audio otherwise.
//#define N23_AUDIO_8_BIT

#ifdef N23_AUDIO_8_BIT
#define N23_AUDIO_SAMPLE_TYPE int8_t
#else
#define N23_AUDIO_SAMPLE_TYPE int16_t
#endif

typedef struct {
    N23_AUDIO_SAMPLE_TYPE r;
    N23_AUDIO_SAMPLE_TYPE l; 
} n23_audio_sample;

// Gives us 10.6ms of buffered audio, so 5.3ms between interrupts.
#define BUFFERED_SAMPLES 256

typedef void (*n23_audio_renderer)(n23_audio_sample* buffer);

void n23_audio_init(void);

void n23_audio_start(n23_audio_renderer initial_renderer);

void n23_audio_set_renderer(n23_audio_renderer new_renderer);

#endif