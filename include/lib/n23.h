#ifndef N23_H
#define N23_H

#include <stdbool.h>
#include <stdint.h>
#include "peripherals/ili9431.h"

void n23_set_up(uint32_t desired_mhz);

void n23_backlight(bool val);
void n23_power_led(bool val);

void n23_set_core_clock(uint32_t desired_mhz);
uint32_t n23_get_core_clock_mhz(void);
#endif
