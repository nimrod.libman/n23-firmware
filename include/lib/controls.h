#ifndef N23_CONTROLS_H
#define N23_CONTROLS_H

#include <stdbool.h>
#include <stdint.h>

typedef enum {
  N23_INPUT_JOY_LEFT_X,
  N23_INPUT_JOY_LEFT_Y,
  N23_INPUT_JOY_RIGHT_X,
  N23_INPUT_JOY_RIGHT_Y,
} n23_input_joy_axis;

void n23_input_buttons_init(void);

bool n23_input_left_l(void);
bool n23_input_left_u(void);
bool n23_input_left_r(void);
bool n23_input_left_d(void);

bool n23_input_right_l(void);
bool n23_input_right_u(void);
bool n23_input_right_r(void);
bool n23_input_right_d(void);

bool n23_input_right_shoulder_inner(void);
bool n23_input_right_shoulder_outer(void);
bool n23_input_left_shoulder_inner(void);
bool n23_input_left_shoulder_outer(void);

bool n23_input_left_select(void);
bool n23_input_right_select(void);
bool n23_input_left_joystick_button(void);
bool n23_input_right_joystick_button(void);

typedef struct {
  uint16_t x;
  uint16_t y;
} n23_input_joy_position;
void n23_input_joy_init(void);
n23_input_joy_position n23_input_left_joy(void);
n23_input_joy_position n23_input_right_joy(void);

#endif