#ifndef MATH_H
#define MATH_H
// Advanced math functions using the CORDIC solvers
// Uses 1q31 Fixed point numbers
// Hardcoded to block for 3 machine cycles per operation for maximum precision (plus ~4 cycles to do register setup)

// If you want to use polling or interrupt modes, or use a DMA, build on top of `cordic.h` like this is

// As a reference, floating point operations take 1-3 cycles each (https://developer.arm.com/documentation/ddi0439/b/Floating-Point-Unit/FPU-Functional-Description/FPU-instruction-set)
// But divide is SUPER expensive! if we do a bunch of those, fixed+CORDIC may come out on top.
// This gives 10-11 bits of precision, 19 for sqrt.

void n23_cordic_math_init(void);

uint32_t n23_cordic_sin(uint32_t rads_div_pi);
uint32_t n23_cordic_cos(uint32_t rads_div_pi);
uint32_t n23_cordic_sinh(uint32_t x);
uint32_t n23_cordic_cosh(uint32_t x);

uint32_t n23_cordic_cartesian_to_angle(uint32_t x, uint32_t y);
uint32_t n23_cordic_pythagoras(uint32_t x, uint32_t y);

uint32_t n23_cordic_atan(uint32_t x);
uint32_t n23_cordic_atanh(uint32_t x);

uint32_t n23_cordic_natural_log(uint32_t x);

uint32_t n23_cordic_sqrt(uint32_t x);


#endif