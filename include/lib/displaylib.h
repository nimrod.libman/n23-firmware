#ifndef N23_DISPLAYLIB_H
#define N23_DISPLAYLIB_H

#define DISPLAY_COLUMNS 320
#define DISPLAY_ROWS    240
#include <stdint.h>

// Takes linebuffer to modify, and line number to render as argument
typedef void (*n23_line_renderer) (uint16_t* buffer, uint16_t line_number);

// Requires timers to have been set up
void n23_displaylib_init(uint32_t desired_spi_clock_mhz);

void n23_displaylib_start(n23_line_renderer initial_renderer);

void n23_displaylib_set_renderer(n23_line_renderer new_renderer);

//TODO resize function to adjust render_{width/height}
#endif

