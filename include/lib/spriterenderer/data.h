// This does not allow a single sprite to act as a background. Consider bumping
// up `width` to a uint16.

#ifndef N23_SPRITES_DATA
#define N23_SPRITES_DATA

#include <stdint.h>

typedef struct {
  uint8_t width;
  uint8_t height;
  uint8_t data[];
} n23_spriterenderer_spritebase;

#endif