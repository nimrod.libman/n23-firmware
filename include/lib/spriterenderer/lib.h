
#ifndef N23_SPRITELIB
#define N23_SPRITELIB
#include "lib/displaylib.h"
#include "lib/spriterenderer/data.h"
#include <stdint.h>

typedef struct {
    n23_spriterenderer_spritebase* base;
    uint16_t x;
    uint16_t y;
    uint16_t* colour_table;
} n23_spriterenderer_sprite;

n23_spriterenderer_sprite sprites[N23_SPRITES_COUNT];

n23_spriterenderer_spritebase null_spritebase = {
    .width = 0,
    .height = 0,
    .data = {},
};

void n23_spriterenderer_init(uint32_t desired_spi_clock_mhz)
{
    n23_displaylib_init(desired_spi_clock_mhz);
    // TODO initialise `sprites` with empty data
}

void renderer(uint16_t* linebuffer, uint16_t row_number)
{
    for (unsigned int i = 0; i < DISPLAY_COLUMNS; i++) {
        linebuffer[i] = 0xffff;
    }
    for (unsigned int i = 0; i < N23_SPRITES_COUNT; i++) {
        n23_spriterenderer_sprite current_sprite = sprites[i];
        if (row_number >= current_sprite.y && row_number < current_sprite.y + current_sprite.base->height) {
            uint8_t sprite_row = row_number - current_sprite.y;
            unsigned int sprite_row_offset = sprite_row * current_sprite.base->width;

            for (unsigned int x = 0; x < current_sprite.base->width; x++) {
                uint8_t colour_index = current_sprite.base->data[sprite_row_offset + x];
                if (colour_index != 0) {
                    uint16_t colour = current_sprite.colour_table[colour_index - 1];
                    linebuffer[current_sprite.x + x] = colour;
                }
            }
        }
    }
}

void n23_spriterenderer_start(void) { n23_displaylib_start(renderer); }

#endif