#ifndef N23_TIMERS_H
#define N23_TIMERS_H

#include "hw/timers/basic.h"
#include "hw/timers/general.h"
#include "hw/timers/systick.h"

void n23_timers_init(void);


void n23_timers_start_ticker(void);

uint32_t n23_timers_ticker(void);

void n23_timers_sleep(uint16_t microseconds);

#endif