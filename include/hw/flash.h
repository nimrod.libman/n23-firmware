#ifndef FLASH_H
#define FLASH_H

#include <stdint.h>

typedef union
{
	struct
	{
		uint32_t latency                    :4;
		uint32_t _res1                      :4;
        uint32_t prefetch                   :1;
        uint32_t instruction_cache_enable   :1;
        uint32_t data_cache_enable          :1;
        uint32_t instruction_cache_reset    :1;
        uint32_t data_cache_reset           :1;
        uint32_t run_power_down             :1;
        uint32_t sleep_power_down           :1;
        uint32_t __res2                     :3;
        uint32_t debug                      :1;
        uint32_t __res3                     :13;
	};

	uint32_t mask;
} stm32_flash_access_control;

typedef struct{
    stm32_flash_access_control access_control;
    uint32_t power_down_key;
    uint32_t key;
    uint32_t option_key;
    uint32_t status;
    uint32_t control;
    uint32_t ecc;
    uint32_t option;
    uint32_t pcrop1_start; 
    uint32_t pcrop1_end; 
    uint32_t wrp_area_a;
    uint32_t wrp_area_b;
    uint32_t securable_area;
} stm32_flash_reg;



static volatile stm32_flash_reg *const stm32_flash = (volatile stm32_flash_reg *)0x40022000; 
#endif