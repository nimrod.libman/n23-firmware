#ifndef STM32_DAC_H
#define STM32_DAC_H



typedef enum{
    stm32_dac_wavegen_NONE      = 0b00,
    stm32_dac_wavegen_NOISE     = 0b01,
    stm32_dac_wavegen_TRIANGLE  = 0b10,
    stm32_dac_wavegen_SAWTOOTH  = 0b11,
} stm32_dac_wavegen;

typedef union {
	struct
	{
		uint32_t enable_1                     : 1;
        uint32_t trigger_enable_1             : 1; // If enabled, data_hold is slower to wreite into data_output
        uint32_t trigger_select_1             : 4; // Which event triggers channel
        uint32_t waveform_1                   : 2;
        uint32_t waveform_amplitude_1         : 4;
        uint32_t dma_enable_1                 : 1;
        uint32_t dma_underrun_irq_enable_1    : 1;
        uint32_t dma_calibration_enable_1     : 1;
        uint32_t __reserved_1                 : 1;		
        uint32_t enable_2                     : 1;
        uint32_t trigger_enable_2             : 1; 
        uint32_t trigger_select_2             : 4; 
        uint32_t waveform_2                   : 2;
        uint32_t waveform_amplitude_2         : 4;
        uint32_t dma_enable_2                 : 1;
        uint32_t dma_underrun_irq_enable_2    : 1;
        uint32_t dma_calibration_enable_2     : 1;
        uint32_t __reserved_2                 : 1;
    };

	uint32_t mask;
} stm32_dac_control;


typedef union {
	struct
	{
		uint32_t trigger_1          : 1;
        uint32_t trigger_2          : 1;
        uint32_t __res1             : 14;
        uint32_t sawtooth_trigger_1 : 1;
        uint32_t sawtooth_trigger_2 : 1;
        uint32_t __res2             : 14;
    };

	uint32_t mask;
} stm32_dac_trigger;


typedef union {
	struct
	{
		uint32_t data               : 12;
        uint32_t __res1             : 4;
        uint32_t double_data        : 12; // Used only in "Double Data" mode
        uint32_t __res2             : 4;
    };

	uint32_t mask;
} stm32_dac_data_12r;

typedef union {
	struct
	{
        uint32_t __res1             : 4;
		uint32_t data               : 12;
        uint32_t __res2             : 4;
        uint32_t double_data        : 12; // Used only in "Double Data" mode
    };

	uint32_t mask;
} stm32_dac_data_12l;

// Does this need to be masked? Given it's byte-aligned could it be a struct?
typedef union {
	struct
	{
		uint32_t data               : 8;
        uint32_t double_data        : 8; // Used only in "Double Data" mode
        uint32_t __reserved         : 16;
    };

	uint32_t mask;
} stm32_dac_data_8;

typedef struct {
	stm32_dac_data_12r  data_12r;
	stm32_dac_data_12l  data_12l;
	stm32_dac_data_8    data_8;
} stm32_dac_data;


typedef union {
	struct
	{
		uint32_t data               : 12;
        uint32_t __res1             : 4;
        uint32_t double_data        : 12; // Used only in "Double Data" mode
        uint32_t __res2             : 4;
    };

	uint32_t mask;
} stm32_dac_output;

typedef union {
	struct
	{
        uint32_t __res1               : 11;
		uint32_t ready_1              : 1;
		uint32_t output_1             : 1; // 0 - `data` is actual DAC out, 1 - `double_data`
        uint32_t underrun_1           : 1;
        uint32_t calibration_offset_1 : 1; // What is this for?
        uint32_t sample_busy_1        : 1;
        uint32_t __res2               : 11;
		uint32_t ready_2              : 1;
		uint32_t output_2             : 1; // 0 - `data` is actual DAC out, 1 - `double_data`
        uint32_t underrun_2           : 1;
        uint32_t calibration_offset_2 : 1; // What is this for?
        uint32_t sample_busy_2        : 1;
    };

	uint32_t mask;
} stm32_dac_status;


typedef union {
	struct
	{
        uint32_t offset_trimming_1     : 5;
		uint32_t __res1                : 11;        
        uint32_t offset_trimming_2     : 5;
		uint32_t __res2                : 11;
    };

	uint32_t mask;
} stm32_dac_calibration;



typedef union {
	struct
	{
		uint32_t channel_mode_1     : 3;
        uint32_t __res1             : 5;
        uint32_t double_data_1      : 1;  
        uint32_t input_signed_1     : 1; // Is this unsigned or 2sc?
        uint32_t __res2             : 4;
        uint32_t frequency          : 2;
        uint32_t channel_mode_2     : 3;
        uint32_t __res3             : 5;
        uint32_t double_data_2      : 1;  
        uint32_t input_signed_2     : 1; // Is this unsigned or 2sc?
        uint32_t __res4             : 6;
    };

	uint32_t mask;
} stm32_dac_mode_control;


typedef union {
	struct
	{
		uint32_t reset              : 12;
        uint32_t direction          : 1;  // 0 - decrement
        uint32_t __reserved         : 5;
        uint32_t increment          : 16;  // 12.4 fixed point
    };

	uint32_t mask;
} stm32_dac_sawtooth;

typedef union {
	struct
	{
		uint32_t reset_1                : 4;
        uint32_t __res1                 : 4; 
        uint32_t increment_1            : 4;  
        uint32_t __res2                 : 4;		
        uint32_t reset_2                : 4;
        uint32_t __res3                 : 4; 
        uint32_t increment_2            : 4;  
        uint32_t __res4                 : 4;
    };

	uint32_t mask;
} stm32_dac_sawtooth_triggers;

typedef struct {
    stm32_dac_control control;
    stm32_dac_trigger trigger;
    stm32_dac_data data_holding[3]; // ch1, ch2, dual
    stm32_dac_output data_output_ch1;
    stm32_dac_output data_output_ch2;
    stm32_dac_status status;
    stm32_dac_calibration calibration;
    stm32_dac_mode_control mode_control;
    uint32_t sample_time_1;
    uint32_t sample_time_2;
    uint32_t sample_time_dual;
    uint32_t sample_refresh_time;
    uint32_t __reserved1;
    uint32_t __reserved2;
    stm32_dac_sawtooth sawtooth_1;
    stm32_dac_sawtooth sawtooth_2;
    stm32_dac_sawtooth_triggers sawtooth_triggers;

} stm32_dac_device;

// Outputs: PA4, PA5
static volatile stm32_dac_device *const stm32_dac1 = (volatile stm32_dac_device *)0x50000800; 
//PA6
static volatile stm32_dac_device *const stm32_dac2 = (volatile stm32_dac_device *)0x50000C00; 


#endif