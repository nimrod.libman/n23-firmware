#ifndef CORDIC_H
#define CORDIC_H
#include <stdint.h>

typedef enum {
    STM32_CORDIC_COS = 0,
    STM32_CORDIC_SIN = 1,
    STM32_CORDIC_PHASE = 2,
    STM32_CORDIC_MODULUS = 3,
    STM32_CORDIC_ATAN = 4,
    STM32_CORDIC_COSH = 5,
    STM32_CORDIC_SINH = 6,
    STM32_CORDIC_ATANH = 7,
    STM32_CORDIC_NATURAL_LOG = 8,
    STM32_CORDIC_SQRT = 9,
} stm32_cordic_functions;

typedef union {
    struct
	{
		stm32_cordic_functions func : 4;
		uint32_t precision          : 4;
		uint32_t scale              : 3;
		uint32_t _res1              : 5;
		uint32_t interrupt_enable   : 1; //An interrupt request is generated whenever the RRDY flag is set
		uint32_t dma_read_enable    : 1;
		uint32_t dma_write_enable   : 1;
		uint32_t nres               : 1; // Number of results in the output buffer (-1) (MUST MAKE THIS NUMBER OF READS TO RESET THE CORDIC TO BE ABLE TO DO ANOTHER OP)
        uint32_t nargs              : 1; // Number of expected args (-1)
		uint32_t result_size        : 1; // 0 - 32 bit outputs, 1 - 16 bit output
		uint32_t arg_size           : 1; // 0 - 32 bit inputs, 1 - 16 bit input
		uint32_t _res2              : 8;
        uint32_t ready              : 1; // 1 - needs data to be read from the register to be reset, 0 - Ready for a new operation

	};

	uint32_t _mask;
} stm32_cordic_control_status;

typedef struct {
    stm32_cordic_control_status control;
    uint32_t   arguments;
    uint32_t   results;
} stm32_cordic_device;

static volatile stm32_cordic_device* const stm32_cordic = (volatile stm32_cordic_device *) 0x40020C00;

#endif