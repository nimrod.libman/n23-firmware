#ifndef STM32_TIMERS_GP_H
#define STM32_TIMERS_GP_H

#include <stdint.h>
#include "hw/timers/common.h"

typedef union {
	struct
	{
		uint32_t enable         : 1;
        uint32_t update_disable : 1;
        uint32_t update_source  : 1;    // 0: everything can trigger an "Update" interrupt/DMA request, 1: only over/underflow or DMA request (??)
        uint32_t one_pulse_mode : 1;    // 1: When the next updaet event happens, the counter will stop counting
        uint32_t direction      : 1;    // 0 - Counter counts up to ther auto-reload value, 1 - counter counts down to 0 from A-R value
        uint32_t center_align_mode : 2; // 0 - Counter counts depenging on `direction` , 1-3: alternates counting up and down, interrupts set at random intervals
        uint32_t auto_reload    : 1;    // 1: Buffers TIM_ARR register (??)
        uint32_t clock_division : 2;    // only affects the filter functionality?
        uint32_t __res2         : 1;
        uint32_t uif_remap      : 1;    // Why would you WANT this feature???
        uint32_t dither         : 1;
        uint32_t __res3         : 3+16;
	};

	uint32_t mask;
} stm32_timer_gp_long_config1;

typedef union {
	struct
	{
		uint32_t enable         : 3;
        uint32_t capture_compare_dma : 1;// 0 - DMA requestrs sent when "Capture/Compare" event occurs (?) , 1 - DMA requests sent when "update" event occurs
        uint32_t master_mode    : 3; // Idk what this is doing...
        uint32_t tim_ti1_select : 1;
        uint32_t __res4         : 17;
        uint32_t master_mode_2  : 1; // AAAA
        uint32_t __res5         : 6;
	};

	uint32_t mask;
} stm32_timer_gp_long_config2;



typedef union {
	struct
	{
		uint32_t update_interrupt          : 1;
		uint32_t capture_compare_interrupt_1 : 1;
		uint32_t capture_compare_interrupt_2 : 1;
		uint32_t capture_compare_interrupt_3 : 1;
		uint32_t capture_compare_interrupt_4 : 1;
        uint32_t __res1            : 1;
        uint32_t trigger_interrupt : 1;
		uint32_t __res2            : 1;
        uint32_t update_dma        : 1;
		uint32_t capture_compare_dma_1 : 1;
		uint32_t capture_compare_dma_2 : 1;
		uint32_t capture_compare_dma_3 : 1;
		uint32_t capture_compare_dma_4 : 1;
		uint32_t __res3            : 1;
        uint32_t trigger_dma       : 1;
		uint32_t __res4            : 1+4;
        uint32_t index             : 1;
        uint32_t direction_change  : 1;
        uint32_t index_error       : 1;
        uint32_t transition_error  : 1;
		uint32_t __res5            : 8;        
	};

	uint32_t mask;
} stm32_timer_gp_interrupts;


typedef union {
	struct
	{
		uint32_t value  : 31;
		uint32_t update_interrupt_pending      : 1;      
	};

	uint32_t mask;
} stm32_timer_gp_long_counter;


typedef union {
	struct
	{
		uint32_t update  : 1;
		uint32_t dont_care      : 31;      
	};

	uint32_t mask;
} stm32_timer_gp_long_event_gen;

typedef union {
	struct
	{
		uint32_t input_cc1s           :2;
        uint32_t input_ic1psc         :2;
        uint32_t input_ic1f           :4;
		uint32_t input_cc2s           :2;
        uint32_t input_ic2psc         :2;
        uint32_t input_ic2f           :4;
        uint32_t __res1             :16;   
	};
    struct
	{
		uint32_t output_cc1s           :2;
        uint32_t output_oc1fe          :1;
        uint32_t output_oc1pe          :1;
        uint32_t output_oc1m           :3;
        uint32_t output_oc1ce          :1;
        uint32_t output_cc2s           :2;
        uint32_t output_oc2fe          :1;
        uint32_t output_oc2pe          :1;
        uint32_t output_oc2m           :3;
        uint32_t output_oc2ce          :1;
        uint32_t output_oc1m3          :1;
        uint32_t __res2                :7;
        uint32_t output_oc2m3          :1;
        uint32_t __res3                :7;
          
	};
	uint32_t mask;
} stm32_timer_gp_long_capture_compare_mode;


typedef union {
	struct
	{
		uint32_t cc1_enable         : 1;
		uint32_t cc1_polarity       : 1;
        uint32_t __res1             : 1;
        uint32_t cc1_n_polarity     : 1;
		uint32_t cc2_enable         : 1;
		uint32_t cc2_polarity       : 1;
        uint32_t __res2             : 1;
        uint32_t cc2_n_polarity     : 1;  
		uint32_t cc3_enable         : 1;
		uint32_t cc3_polarity       : 1;
        uint32_t __res3             : 1;
        uint32_t cc3_n_polarity     : 1; 
		uint32_t cc4_enable         : 1;
		uint32_t cc4_polarity       : 1;
        uint32_t __res4             : 1;
        uint32_t cc4_n_polarity     : 1;
        uint32_t __res5             : 16;       
	};

	uint32_t mask;
} stm32_timer_gp_capture_compare_enable;

typedef struct{
    stm32_timer_gp_long_config1 config1;
    stm32_timer_gp_long_config2 config2;
    uint32_t slave_control; // TODO CBA implementing this one yet...
    stm32_timer_gp_interrupts interrupt_enable;
    stm32_timer_gp_interrupts interrupt_status;
    stm32_timer_gp_long_event_gen generate_event;
    stm32_timer_gp_long_capture_compare_mode capture_compare_mode[2];
    stm32_timer_gp_capture_compare_enable capture_compare_enable;
    stm32_timer_gp_long_counter counter;
    stm32_timer_prescaler prescaler; 
    uint32_t auto_reload;
    uint32_t __reserved;
    uint32_t capture_compare[4];
    uint32_t encoder; // Not implemneted yet
    uint32_t input_selection; // Not implemneted yet
    uint32_t alternate_function[2]; // Not implemneted yet
    uint32_t dma_control; // Not implemneted yet    
    uint32_t dma_addr; // Not implemneted yet
}stm32_timer_gp_long;

// Reserved as a monotonic counter
static volatile stm32_timer_gp_long *const stm32_tim2 = (volatile stm32_timer_gp_long *)0x40000000; 

// Below can freely be used
static volatile stm32_timer_gp_long *const stm32_tim3 = (volatile stm32_timer_gp_long *)0x40000400; 
static volatile stm32_timer_gp_long *const stm32_tim4 = (volatile stm32_timer_gp_long *)0x40000800; 
static volatile stm32_timer_gp_long *const stm32_tim5 = (volatile stm32_timer_gp_long *)0x40000C00; 

#endif
