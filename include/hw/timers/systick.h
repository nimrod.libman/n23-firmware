#ifndef STM32_TIMERS_SYSTICK_H
#define STM32_TIMERS_SYSTICK_H


typedef union {
	struct
	{
		uint32_t enable         : 1;
		uint32_t interrupt      : 1;     
        uint32_t source         : 1; // 0 AHB/8, 1: processor clock (AHB)   (I assume this means scaling?)
		uint32_t __res1         : 13;  
		uint32_t countflag      : 1;  //Retunrs 1 if the counter counted down to 0 since last read
		uint32_t __res2         : 15;  
	};

	uint32_t mask;
}  stm32_timer_systick_control;

typedef union {
	struct
	{
		uint32_t calibration    : 23;
		uint32_t __res1         : 6;    
		uint32_t skew           : 1; // indicates if `calibration` is exact (?)   
		uint32_t noref          : 1; // Indicates if alternate clock is used
	};

	uint32_t mask;
}  stm32_timer_systick_calibration;


typedef struct {  
    stm32_timer_systick_control control;  
    uint32_t reload;        // ACSHUALLY 24 bit
    uint32_t current_value; // ACKSHUALLY 24 bit
    stm32_timer_systick_calibration calibration;
} stm32_timer_systick;

static volatile stm32_timer_systick *const stm32_systick = (volatile stm32_timer_systick *)0xE000E010; 

#endif