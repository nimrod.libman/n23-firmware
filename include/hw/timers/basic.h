#ifndef STM32_TIMERS_BASIC_H
#define STM32_TIMERS_BASIC_H

#include <stdint.h>
#include "hw/timers/common.h"
typedef union {
	struct
	{
		uint32_t enable         : 1;
        uint32_t update_disable : 1;
        uint32_t update_source  : 1;    // 0: everything can trigger an "Update" interrupt/DMA request, 1: only over/underflow or DMA request (??)
        uint32_t one_pulse_mode : 1;    // 1: When the next updaet event happens, the counter will stop counting
        uint32_t __res1         : 3;
        uint32_t auto_reload    : 1;    // 1: Buffers TIM_ARR register (??)
        uint32_t __res2         : 3;
        uint32_t uif_remap      : 1;    // Why would you WANT this feature???
        uint32_t dither         : 1;
        uint32_t __res3         : 3+16;
	};

	uint32_t mask;
} stm32_timer_basic_config1;



typedef union {
	struct
	{
		uint32_t __res1         : 4;
        uint32_t master_mode    : 3;
        uint32_t __res2         : 9+16;
	};

	uint32_t mask;
} stm32_timer_basic_config2;

typedef union {
	struct
	{
		uint32_t update: 1;
		uint32_t __res1          : 7;
		uint32_t dma      : 1;
		uint32_t __res2          : 7+16;
        
	};

	uint32_t mask;
} stm32_timer_basic_interrupts;


typedef union {
	struct
	{
		uint32_t update          : 1;
		uint32_t __res2          : 31;
        
	};

	uint32_t mask;
} stm32_timer_basic_status;

typedef union {
	struct
	{
		uint32_t value  : 16;
		uint32_t __res1 : 15;
		uint32_t update_interrupt_pending      : 1;        
	};

	uint32_t mask;
} stm32_timer_basic_counter;

typedef struct{
    stm32_timer_basic_config1 config1;
    stm32_timer_basic_config2 config2;
    uint32_t __reserved1;
    stm32_timer_basic_interrupts interrupts_enable;
    stm32_timer_basic_status interrupt_status; 
    stm32_timer_basic_status generate_event; 
    uint32_t __reserved2[3];
    stm32_timer_basic_counter counter;
    stm32_timer_prescaler prescaler; // Man I could simplify a bit by making these just a uint32 and advise caution...
    stm32_timer_autoreload auto_reload; 
}stm32_timer_basic;

// Reserved for use for software delays
static volatile stm32_timer_basic *const stm32_tim6 = (volatile stm32_timer_basic *)0x40001000; 

// Reserved for use by the audio system
static volatile stm32_timer_basic *const stm32_tim7 = (volatile stm32_timer_basic *)0x40001400; 
#endif