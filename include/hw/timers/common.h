#ifndef STM32_TIMERS_COMMON_H
#define STM32_TIMERS_COMMON_H


typedef union {
	struct
	{
		uint32_t prescaler  : 16;
		uint32_t __res1     : 16;       
	};

	uint32_t mask;
} stm32_timer_prescaler;

typedef union {
	struct
	{
		uint32_t auto_reload: 20;
		uint32_t __res1     : 12;       
	};

	uint32_t mask;
} stm32_timer_autoreload;

#endif