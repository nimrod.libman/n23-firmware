#ifndef HW_H
#define HW_H

#include <stdbool.h>
#include <stdint.h>

/// Should never be used by consumers. For internal use only
static inline void stm32_register_set(volatile uint32_t *reg, uint8_t size, uint8_t index, uint32_t value) {
  uint8_t offset = (index * size);
  uint32_t mask = ((1U << size) - 1) << offset;
  *reg = (*reg & ~mask) | (value << offset);
}

static inline uint32_t stm32_register_get(volatile uint32_t *reg, uint8_t size, uint8_t index) {
  uint8_t offset = (index * size);
  uint32_t mask = ((1U << size) - 1) << offset;
  uint32_t value = (*reg & mask) >> offset;
  return value;
}




#define populate_reg_1bit(name, t)                                             \
  union {                                                                      \
    struct {                                                                   \
      t name##_0 : 1;                                                          \
      t name##_1 : 1;                                                          \
      t name##_2 : 1;                                                          \
      t name##_4 : 1;                                                          \
      t name##_5 : 1;                                                          \
      t name##_6 : 1;                                                          \
      t name##_7 : 1;                                                          \
      t name##_8 : 1;                                                          \
      t name##_9 : 1;                                                          \
      t name##_10 : 1;                                                         \
      t name##_11 : 1;                                                         \
      t name##_12 : 1;                                                         \
      t name##_13 : 1;                                                         \
      t name##_14 : 1;                                                         \
      t name##_15 : 1;                                                         \
      uint32_t empty : 16;                                                     \
    };                                                                         \
    uint32_t mask;                                                             \
  }
#define populate_reg_2bit(name, t)                                             \
  union {                                                                      \
    struct {                                                                   \
      t name##_0 : 2;                                                          \
      t name##_1 : 2;                                                          \
      t name##_2 : 2;                                                          \
      t name##_4 : 2;                                                          \
      t name##_5 : 2;                                                          \
      t name##_6 : 2;                                                          \
      t name##_7 : 2;                                                          \
      t name##_8 : 2;                                                          \
      t name##_9 : 2;                                                          \
      t name##_10 : 2;                                                         \
      t name##_11 : 2;                                                         \
      t name##_12 : 2;                                                         \
      t name##_13 : 2;                                                         \
      t name##_14 : 2;                                                         \
      t name##_15 : 2;                                                         \
    };                                                                         \
    uint32_t mask;                                                             \
  }


typedef uint32_t stm32_gpio_set_reset;
typedef uint32_t stm32_gpio_lock;

// Combines the AF_LOW and AF_HIGH registers
// TODO break it up into two registers boooo
typedef union {
  struct {
    uint32_t af_0 : 4;
    uint32_t af_1 : 4;
    uint32_t af_2 : 4;
    uint32_t af_4 : 4;
    uint32_t af_5 : 4;
    uint32_t af_6 : 4;
    uint32_t af_7 : 4;
    uint32_t af_8 : 4;
    uint32_t af_9 : 4;
    uint32_t af_10 : 4;
    uint32_t af_11 : 4;
    uint32_t af_12 : 4;
    uint32_t af_13 : 4;
    uint32_t af_14 : 4;
    uint32_t af_15 : 4;
  };
  struct {
    uint32_t mask_low;
    uint32_t mask_high;
  };
} stm32_gpio_af;

typedef populate_reg_1bit(br, bool) stm32_gpio_reset;


#define STM32_RCC 0x40021000
// Don't care about low power, reset, and RTC registers

// TODO (RCC_CRRCR) will be relevant once we start using USB.

typedef union{
  struct
	{
		uint32_t uart1  : 2;
		uint32_t uart2  : 2;
		uint32_t uart3  : 2;
		uint32_t uart4  : 2;
		uint32_t uart5  : 2;  
		uint32_t lpuart1: 2; 
		uint32_t i2c1   : 2; 
		uint32_t i2c2   : 2; 
		uint32_t i2c3   : 2; 
		uint32_t lptim1 : 2; 
		uint32_t sai1   : 2; 
		uint32_t i2s3   : 2; 
		uint32_t fdcan  : 2; 
		uint32_t clk48  : 2; 
		uint32_t adc12  : 2; 
		uint32_t adc345 : 2; 
	};
  uint32_t mask;
} stm32_rcc_independent_clock_config;



typedef union{
  struct
	{
    uint32_t source                   :2;
    uint32_t __res1                   :2;
    uint32_t input_division_factor    :4;
    uint32_t vco_multiplication_factor:7;
    uint32_t __res2                   :1;
    uint32_t p_clock_enable           :1;
    uint32_t p_clock_division_factor  :1;
    uint32_t __res3                   :2;
    uint32_t q_clock_enable           :1;
    uint32_t q_clock_division_factor  :2;
    uint32_t __res4                   :1;
    uint32_t r_clock_enable           :1;
    uint32_t r_clock_division_factor  :2;
    uint32_t pllpdiv                  :5;

	};
  uint32_t mask;
} stm32_rcc_pll_config;

typedef union{
  struct
	{
    uint32_t system_clock             :2;
    uint32_t system_clock_status      :2;
    uint32_t ahb_prescale             :3;
    uint32_t apb_prescale             :3;
    uint32_t __res1                   :10;
    uint32_t clock_output             :4;
    uint32_t clock_out_prescale       :4;
    uint32_t __res2                   :1;  
	};
  uint32_t mask;
} stm32_rcc_clock_config;


typedef union{
  struct
	{
    uint32_t __res1                   :8;
    uint32_t hsi16_enable             :1;
    uint32_t hsi16_kernel_enable      :1;
    uint32_t hsi16_ready              :1;
    uint32_t __res2                   :5;
    uint32_t hse_enable               :1;
    uint32_t hse_ready                :1;
    uint32_t hse_bypass               :1;
    uint32_t security                 :1;
    uint32_t __res3                   :4;
    uint32_t pll_enable               :1;
    uint32_t pll_ready                :1;
    uint32_t __res4                   :6;
	};
  uint32_t mask;
} stm32_rcc_control;


typedef struct{
  stm32_rcc_control control;
  uint32_t internal_clock_sources;
  stm32_rcc_clock_config config;
  stm32_rcc_pll_config pll_config;
  uint32_t __res1[2];
  uint32_t interrupt_enable;
  uint32_t interrupt_flag;
  uint32_t interrupt_clear;
  uint32_t __res2;
  uint32_t peripheral_reset[7];
  uint32_t __res3;
  uint32_t peripheral_enable[7];
  uint32_t __res4;
  uint32_t peripheral_enable_in_sleep[7];
  uint32_t __res5;
  stm32_rcc_independent_clock_config independent_clock_config;
  uint32_t bdc;
  uint32_t status_recovery;
  uint32_t clock_recovery;
  uint32_t independent_clock_config_2;
} stm32_rcc_regs;

static volatile stm32_rcc_regs *const stm32_rcc = (volatile stm32_rcc_regs*  ) STM32_RCC;



typedef struct{
  uint32_t pull_up;
  uint32_t pull_down; 
} stm32_power_pull_control;

typedef union{
  struct
	{
    uint32_t __res1                   :8;
    uint32_t range_1_mode             :1;
    uint32_t __res2                   :23;
	};
  uint32_t mask;
} stm32_power_control_5;


typedef struct{
  uint32_t control_1;
  uint32_t control_2;
  uint32_t control_3;
  uint32_t control_4;
  uint32_t status_1;
  uint32_t status_2;
  uint32_t status_clear;
  stm32_power_pull_control pull_control[7];
  stm32_power_control_5 control_5; 
} stm32_power_regs;

static volatile stm32_power_regs *const stm32_power = (volatile stm32_power_regs*  ) 0x40007000;


typedef enum {
  stm32_peripheral_enable_CRC =       0 * 32 + 12,
  stm32_peripheral_enable_FLASH =     0 * 32 + 8,
  stm32_peripheral_enable_FMAC =      0 * 32 + 4,
  stm32_peripheral_enable_CORDIC =    0 * 32 + 3,
  stm32_peripheral_enable_DMA1MUX =   0 * 32 + 2,
  stm32_peripheral_enable_DMA2 =      0 * 32 + 1,
  stm32_peripheral_enable_DMA1 =      0 * 32 + 0,

  stm32_peripheral_enable_RNG =       1 * 32 + 26,
  stm32_peripheral_enable_AES =       1 * 32 + 24,
  stm32_peripheral_enable_DAC4=       1 * 32 + 19,
  stm32_peripheral_enable_DAC3=       1 * 32 + 18,
  stm32_peripheral_enable_DAC2=       1 * 32 + 17,
  stm32_peripheral_enable_DAC1=       1 * 32 + 16,
  stm32_peripheral_enable_ADC345 =    1 * 32 + 14,
  stm32_peripheral_enable_ADC12 =     1 * 32 + 13,
  stm32_peripheral_enable_GPIOG =     1 * 32 + 6,
  stm32_peripheral_enable_GPIOF =     1 * 32 + 5,
  stm32_peripheral_enable_GPIOE =     1 * 32 + 4,
  stm32_peripheral_enable_GPIOD =     1 * 32 + 3,
  stm32_peripheral_enable_GPIOC =     1 * 32 + 2,
  stm32_peripheral_enable_GPIOB =     1 * 32 + 1,
  stm32_peripheral_enable_GPIOA =     1 * 32 + 0,

  stm32_peripheral_enable_QSPI =      2 * 32 + 8,
  stm32_peripheral_enable_FMC =       2 * 32 + 0,

  stm32_peripheral_enable_LPTIM =     4 * 32 + 31,
  stm32_peripheral_enable_I2C3 =      4 * 32 + 30,
  stm32_peripheral_enable_PWR =       4 * 32 + 28,
  stm32_peripheral_enable_FDCAN=      4 * 32 + 25,
  stm32_peripheral_enable_USB =       4 * 32 + 23,
  stm32_peripheral_enable_I2C2=       4 * 32 + 22,
  stm32_peripheral_enable_I2C1   =    4 * 32 + 21,
  stm32_peripheral_enable_UART5 =     4 * 32 + 20,
  stm32_peripheral_enable_UART4 =     4 * 32 + 19,
  stm32_peripheral_enable_USART3=     4 * 32 + 18,
  stm32_peripheral_enable_USART2=     4 * 32 + 17,
  stm32_peripheral_enable_SPI3 =      4 * 32 + 15,
  stm32_peripheral_enable_SPI2 =      4 * 32 + 14,
  stm32_peripheral_enable_WWDG =      4 * 32 + 11,
  stm32_peripheral_enable_RTCAPB=     4 * 32 + 10,
  stm32_peripheral_enable_TIM7=       4 * 32 + 5,
  stm32_peripheral_enable_TIM6=       4 * 32 + 4,
  stm32_peripheral_enable_TIM5=       4 * 32 + 3,
  stm32_peripheral_enable_TIM4=       4 * 32 + 2,
  stm32_peripheral_enable_TIM3=       4 * 32 + 1,
  stm32_peripheral_enable_TIM2=       4 * 32 + 0,

  stm32_peripheral_enable_UCPD1=      5 * 32 + 8,
  stm32_peripheral_enable_I2C4 =      5 * 32 + 1,
  stm32_peripheral_enable_LPUART1 =   5 * 32 + 0,


  stm32_peripheral_enable_HRTIM1=     6 * 32 + 26,
  stm32_peripheral_enable_SAI1 =      6 * 32 + 21,
  stm32_peripheral_enable_TIM20 =     6 * 32 + 20,
  stm32_peripheral_enable_TIM17 =     6 * 32 + 18,
  stm32_peripheral_enable_TIM16=      6 * 32 + 17,
  stm32_peripheral_enable_TIM15=      6 * 32 + 16,
  stm32_peripheral_enable_SPI4 =      6 * 32 + 15,
  stm32_peripheral_enable_USART1=     6 * 32 + 14,
  stm32_peripheral_enable_TIM8    =   6 * 32 + 13,
  stm32_peripheral_enable_SPI1 =      6 * 32 + 12,
  stm32_peripheral_enable_TIM1 =      6 * 32 + 11,
  stm32_peripheral_enable_SYSCFG  =   6 * 32 + 0,
} stm32_peripheral_enable;

static inline void stm32_clock_peripheral(stm32_peripheral_enable peripheral, bool value){
  stm32_register_set(&stm32_rcc->peripheral_enable[peripheral/32], 1 , peripheral % 32, value);
}


#define DEFAULT_CORE_CLOCK 16000000


//TODO perhaps move this bit out to an `nvic` header?
typedef enum {
  stm32_g4_interrupt_wwdg       = 0,
  stm32_g4_interrupt_pvd_pwm    = 1,
  stm32_g4_interrupt_rtc_tamp_css=2,
  stm32_g4_interrupt_rtc_wkup   = 3,
  stm32_g4_interrupt_flash      = 4,
  stm32_g4_interrupt_rcc        = 5,
  stm32_g4_interrupt_exti0      = 6,
  stm32_g4_interrupt_exti1      = 7,
  stm32_g4_interrupt_exti2      = 8,
  stm32_g4_interrupt_exti3      = 9,
  stm32_g4_interrupt_exti4      = 10,
  stm32_g4_interrupt_dma1_ch1   = 11,
  stm32_g4_interrupt_dma1_ch2   = 12,
  stm32_g4_interrupt_dma1_ch3   = 13,
  stm32_g4_interrupt_dma1_ch4   = 14,
  stm32_g4_interrupt_dma1_ch5   = 15,
  stm32_g4_interrupt_dma1_ch6   = 16,
  stm32_g4_interrupt_dma1_ch7   = 17,
  stm32_g4_interrupt_adc1_2     = 18,
  stm32_g4_interrupt_usb_hp     = 19,
  stm32_g4_interrupt_usb_lp     = 20,
  stm32_g4_interrupt_fdcan1_it0 = 21,
  stm32_g4_interrupt_fdcan1_it1 = 22,
  stm32_g4_interrupt_exti9_5    = 23,
  stm32_g4_interrupt_tim1_brk_tim5 = 24,
  stm32_g4_interrupt_tim1_up_tim16 = 25,
  stm32_g4_interrupt_tim1_trg_com_tim17_tim1_dir_tim1_idx    = 26,
  stm32_g4_interrupt_tim1_cc    = 27,
  stm32_g4_interrupt_tim2       = 28,
  stm32_g4_interrupt_tim3       = 29,
  stm32_g4_interrupt_tim4       = 30,
  stm32_g4_interrupt_i2c1_ev    = 31,
  stm32_g4_interrupt_i2c1_er    = 32,
  stm32_g4_interrupt_i2c2_ev    = 33,
  stm32_g4_interrupt_i2c2_er    = 34,
  stm32_g4_interrupt_spi1       = 35,
  stm32_g4_interrupt_spi2       = 36,
  stm32_g4_interrupt_usart1     = 37,
  stm32_g4_interrupt_usart2     = 38,
  stm32_g4_interrupt_usart3     = 39,
  stm32_g4_interrupt_exti_15_10 = 40,
  stm32_g4_interrupt_rtc_alarm  = 41,
  stm32_g4_interrupt_usbwakeup  = 42,
  stm32_g4_interrupt_tim8_brk_tim8_terr_tim8_1err   = 43,
  stm32_g4_interrupt_tim8_up    = 44,
  stm32_g4_interrupt_tim8_trg_com_tim8_dir_tim8_idx = 45,
  stm32_g4_interrupt_tim8_cc    = 46,
  stm32_g4_interrupt_adc3       = 47,
  stm32_g4_interrupt_fsmc       = 48,
  stm32_g4_interrupt_lptim1     = 49,
  stm32_g4_interrupt_tim5       = 50,
  stm32_g4_interrupt_spi3       = 51,
  stm32_g4_interrupt_usart4     = 52,
  stm32_g4_interrupt_usart5     = 53,
  stm32_g4_interrupt_tim6_dacunder = 54,
  stm32_g4_interrupt_tim7_dacunder = 55,
  stm32_g4_interrupt_dma2_ch1   = 56,
  stm32_g4_interrupt_dma2_ch2   = 57,
  stm32_g4_interrupt_dma2_ch3   = 58,
  stm32_g4_interrupt_dma2_ch4   = 59,
  stm32_g4_interrupt_dma2_ch5   = 60,
  stm32_g4_interrupt_adc4       = 61,
  stm32_g4_interrupt_adc5       = 62,
  stm32_g4_interrupt_ucpd1_global= 63,
  stm32_g4_interrupt_comp1_2_3  = 64,
  stm32_g4_interrupt_comp4_5_6  = 65,
  stm32_g4_interrupt_comp7      = 66,
  stm32_g4_interrupt_hrtim_master= 67,
  stm32_g4_interrupt_hrtim_tim_a= 68,
  stm32_g4_interrupt_hrtim_tim_b= 69,
  stm32_g4_interrupt_hrtim_tim_c= 70,
  stm32_g4_interrupt_hrtim_tim_d= 71,
  stm32_g4_interrupt_hrtim_tim_e= 72,
  stm32_g4_interrupt_hrtim_tim_flt = 73,
  stm32_g4_interrupt_hrtim_tim_f= 74,
  stm32_g4_interrupt_crs        = 75,
  stm32_g4_interrupt_sai        = 76,
  stm32_g4_interrupt_tim20_brk_terr_ierr    = 77,
  stm32_g4_interrupt_tim20_up   = 78,
  stm32_g4_interrupt_tim20_trg_com_dir_idx   = 79,
  stm32_g4_interrupt_tim20_cc   = 80,
  stm32_g4_interrupt_fpu        = 81,
  stm32_g4_interrupt_i2c4_ev    = 82,
  stm32_g4_interrupt_i2c4_er    = 83,
  stm32_g4_interrupt_spi4       = 84,
  stm32_g4_interrupt_aes        = 85,
  stm32_g4_interrupt_fdcan2_it0 = 86,
  stm32_g4_interrupt_fdcan2_it1 = 87,
  stm32_g4_interrupt_fdcan3_it0 = 88,
  stm32_g4_interrupt_fdcan3_it1 = 89,
  stm32_g4_interrupt_rng        = 90,
  stm32_g4_interrupt_lpuart     = 91,
  stm32_g4_interrupt_i2c3_ev    = 92,
  stm32_g4_interrupt_i2c3_er    = 93,
  stm32_g4_interrupt_dmamux_ovr = 94,
  stm32_g4_interrupt_quadspi    = 95,
  stm32_g4_interrupt_dma1_ch8   = 96,
  stm32_g4_interrupt_dma2_ch6   = 97,
  stm32_g4_interrupt_dma2_ch7   = 98,
  stm32_g4_interrupt_dma2_ch8   = 99,
  stm32_g4_interrupt_cordic    = 100,
  stm32_g4_interrupt_fmac      = 101,
}stm32_g4_interrupt;


static volatile uint32_t * const CORTEX_M4_INTERRUPT_SET_ENABLE = (volatile uint32_t *)0xE000E100;
static volatile uint32_t * const CORTEX_M4_INTERRUPT_CLEAR_ENABLE = (volatile uint32_t *)0xE000E180;
static volatile uint32_t * const CORTEX_M4_INTERRUPT_SET_PENDING = (volatile uint32_t *)0xE000E200;
static volatile uint32_t * const CORTEX_M4_INTERRUPT_CLEAR_PENDING = (volatile uint32_t *)0xE000E280;
 
static inline void cortex_m4_nvic_poke(volatile uint32_t* reg, uint8_t x){
  reg[x/32] = (0b1<<(x%32)); // my only hope
}


static inline void cortex_m4_nvic_enable_interrupt(stm32_g4_interrupt interrupt){
  cortex_m4_nvic_poke(CORTEX_M4_INTERRUPT_SET_ENABLE,interrupt);
}

static inline void cortex_m4_nvic_disable_interrupt(stm32_g4_interrupt interrupt){
  cortex_m4_nvic_poke(CORTEX_M4_INTERRUPT_CLEAR_ENABLE,interrupt);
}

static inline void cortex_m4_nvic_pend_interrupt(stm32_g4_interrupt interrupt){
  cortex_m4_nvic_poke(CORTEX_M4_INTERRUPT_SET_PENDING,interrupt);
}
static inline void cortex_m4_nvic_unpend_interrupt(stm32_g4_interrupt interrupt){
  cortex_m4_nvic_poke(CORTEX_M4_INTERRUPT_CLEAR_PENDING,interrupt);
}

//For now, priority-setting isn't in. May be useful later, but not rn.

#endif
