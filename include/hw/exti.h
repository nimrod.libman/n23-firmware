#ifndef STM32_EXTI_H
#define STM32_EXTI_H


typedef union {
    struct
	{
		uint32_t exti0                    :4;
		uint32_t exti1                    :4;
		uint32_t exti2                    :4;
		uint32_t exti3                    :4;
		uint32_t __reserved               :16;
	};

	uint32_t mask;
} stm32_system_config_interrupts;


typedef struct {
    uint32_t memrmp;
    uint32_t cfgr1;
    stm32_system_config_interrupts exticr[4];
    uint32_t scsr;
    uint32_t cfgr2;
    uint32_t swpr;
    uint32_t skr;
    
} stm32_system_config;


static volatile stm32_system_config *const stm32_syscfg = (volatile stm32_system_config *)  0x40010000;

typedef struct {
    uint32_t interrupt_mask_1;
    uint32_t event_mask_1;
    uint32_t rising_trigger_sel_1;
    uint32_t falling_trigger_sel_1;
    uint32_t generate_interrupt_sel_1;
    uint32_t pending_interupt_1;
    uint32_t __reserved[2];
    uint32_t interrupt_mask_2;
    uint32_t event_mask_2;
    uint32_t rising_trigger_sel_2;
    uint32_t falling_trigger_sel_2;
    uint32_t generate_interrupt_sel_2;
    uint32_t pending_interupt_2;
} stm32_external_interrupts;

static volatile stm32_external_interrupts *const stm32_exti = (volatile stm32_external_interrupts *) 0x40010400;

#endif