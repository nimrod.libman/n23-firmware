#ifndef STM32_GPIO
#define STM32_GPIO

#include "stm32g4.h"


typedef enum {
  stm32_pin_mode_Input = 0b00,
  stm32_pin_mode_Output = 0b01,
  stm32_pin_mode_Alternate = 0b10,
  stm32_pin_mode_Analog = 0b11,
} stm32_pin_mode;

typedef enum {
  stm32_pin_type_PushPull = 0b00,
  stm32_pin_type_OpenDrain = 0b01,
} stm32_pin_type;

typedef enum {
  stm32_pin_speed_Low = 0b00,
  stm32_pin_speed_Medium = 0b01,
  stm32_pin_speed_High = 0b10,
  stm32_pin_speed_VeryHigh = 0b11,
} stm32_pin_speed;

typedef enum {
  stm32_pin_pull_None = 0b00,
  stm32_pin_pull_Up = 0b01,
  stm32_pin_pull_Down = 0b10,
  // 11 is reserved
} stm32_pin_pull;


typedef populate_reg_2bit(mode, stm32_pin_mode) stm32_gpio_mode;
typedef populate_reg_1bit(ot, stm32_pin_type) stm32_gpio_type;
typedef populate_reg_2bit(ospeed, stm32_pin_speed) stm32_gpio_speed;
typedef populate_reg_2bit(pupd, stm32_pin_pull) stm32_gpio_pupd;
typedef populate_reg_1bit(id, bool) stm32_gpio_in;
typedef populate_reg_1bit(od, bool) stm32_gpio_out;

typedef struct {
  stm32_gpio_mode mode;     // Pin Mode (Input, GPO, AF, Analog (? Reset???))
  stm32_gpio_type ot;       // Output type (0: push-pull (???), 1: open-drain)
  stm32_gpio_speed ospeed;  // Pin I/O speed (Just O?)
  stm32_gpio_pupd pupd;     // Pull-up/Pull-down
  stm32_gpio_in id;         // Input data (Read this register when a pin is an input)
  stm32_gpio_out od;        // Output data (Do we write to this?)
  stm32_gpio_set_reset bsr; // Bit set/reset
  stm32_gpio_lock lck;      // Pin config lock
  stm32_gpio_af af;         // Alternate function settings
  stm32_gpio_reset br;      // Bit Reset (how is this distinct from BSR?)
} stm32_gpio;

static inline void stm32_gpio_mode_set(volatile stm32_gpio *port, uint8_t index, stm32_pin_mode mode) {
  stm32_register_set(&port->mode.mask, 2, index, mode);
}

static inline void stm32_gpio_type_set(volatile stm32_gpio *port, uint8_t index, stm32_pin_type type) {
  stm32_register_set(&port->ot.mask, 1, index, type);
}

static inline void stm32_gpio_speed_set(volatile stm32_gpio *port, uint8_t index, stm32_pin_speed ospeed) {
  stm32_register_set(&port->ospeed.mask, 2, index, ospeed);
}

static inline void stm32_gpio_pupd_set(volatile stm32_gpio *port, uint8_t index, stm32_pin_pull pupd) {
  stm32_register_set(&port->pupd.mask, 2, index, pupd);
}

static inline void stm32_gpio_out_set(volatile stm32_gpio *port, uint8_t index, bool val) {
  stm32_register_set(&port->od.mask, 1, index, val);
}

static inline void stm32_gpio_af_set(volatile stm32_gpio *port, uint8_t index, uint8_t af) {
  if (index >= 8) {
    stm32_register_set(&port->af.mask_high, 4, index - 8, af);
  } else {
    stm32_register_set(&port->af.mask_low, 4, index, af);
  }
}

static inline bool stm32_gpio_in_get(volatile stm32_gpio *port, uint8_t index) {
  return stm32_register_get(&port->id.mask, 1, index);
}


#define STM32_GPIOA 0x48000000
#define STM32_GPIOB 0x48000400
#define STM32_GPIOC 0x48000800
#define STM32_GPIOD 0x48000C00
#define STM32_GPIOE 0x48001000
#define STM32_GPIOF 0x48001400
#define STM32_GPIOG 0x48001800

static volatile stm32_gpio *const stm32_gpioa = (volatile stm32_gpio *)STM32_GPIOA;
static volatile stm32_gpio *const stm32_gpiob = (volatile stm32_gpio *)STM32_GPIOB;
static volatile stm32_gpio *const stm32_gpioc = (volatile stm32_gpio *)STM32_GPIOC;
static volatile stm32_gpio *const stm32_gpiod = (volatile stm32_gpio *)STM32_GPIOD;
static volatile stm32_gpio *const stm32_gpioe = (volatile stm32_gpio *)STM32_GPIOE;
static volatile stm32_gpio *const stm32_gpiof = (volatile stm32_gpio *)STM32_GPIOF;
static volatile stm32_gpio *const stm32_gpiog = (volatile stm32_gpio *)STM32_GPIOG;


#endif