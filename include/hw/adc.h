#ifndef ADC_H
#define ADC_H

typedef union{
    struct
	{
        uint32_t ready              : 1;
        uint32_t end_of_sampling    : 1;
        uint32_t end_of_conversion  : 1;
        uint32_t end_of_sequence    : 1;
        uint32_t overrun            : 1;
        uint32_t end_of_conversion_injected :1;
        uint32_t end_of_sequence_injected   :1;
        uint32_t watchdog_1         : 1;
        uint32_t watchdog_2         : 1;
        uint32_t watchdog_3         : 1;
        uint32_t injected_ctx_queue_overflow :1;
        uint32_t __reserved: 5+16;
    };
    uint32_t mask;
} stm32_adc_interrupt_status;



typedef union{
    struct
	{
        uint32_t enable             : 1;
        uint32_t disable            : 1;
        uint32_t start_regular      : 1;
        uint32_t start_injected     : 1;
        uint32_t stop_regular       : 1;
        uint32_t stop_injected      : 1;
        uint32_t __reserved         : 22;
        uint32_t voltage_regulator  : 1;
        uint32_t deep_power_down    : 1;
        uint32_t differential_mode  : 1;
        uint32_t calibration        : 1;
    };
    uint32_t mask;
} stm32_adc_control;


typedef union{
    struct
	{
        uint32_t dma_enable         : 1;
        uint32_t dma_circular       : 1; // 0 - oneshot, 1 - circular mode
        uint32_t __reserved         : 1;
        uint32_t resolution         : 2; // 00 - 12bits, 01 - 10 bits, 10 - 8 bits, 11 - 6 bits
        uint32_t external_trigger   : 5;
        uint32_t external_trigger_enable: 2;
        uint32_t overrun_overwrite  : 1;
        uint32_t continuous_conversion:1;
        uint32_t delayed_conversion : 1;
        uint32_t left_align         : 1; // 0 - data register right aligned
        uint32_t discontinuous_mode : 1;
        uint32_t discontinuous_count: 3;
        uint32_t discontinuous_injected:1;
        uint32_t empty_queue_emptied: 1;
        uint32_t watchdog_1_single  : 1; // 0 - enabled on all channel
        uint32_t watchdog_1_regular_chan : 1;
        uint32_t watchdog_1_injected_chan : 1;
        uint32_t auto_injected_group_conversion : 1;
        uint32_t watchdog_channel   : 5;
        uint32_t injected_queue_disable : 1;
    };
    uint32_t mask;
} stm32_adc_config1;

typedef union{
    struct
	{
        uint32_t regular_oversampling_enable    : 1;
        uint32_t injected_oversampling          : 1;
        uint32_t oversampling_ratio             : 3;
        uint32_t oversampling_shift             : 4;
        uint32_t triggered_oversampling         : 1;
        uint32_t regular_oversampling           : 1;
        uint32_t __res1                         : 5;
        uint32_t gain_compensation              : 1;
        uint32_t __res2                         : 8;
        uint32_t trigger_starts_sampling        : 1;
        uint32_t bulb_sampling                  : 1;
        uint32_t sampling_time_control_trigger  : 1;
        uint32_t __res3                         : 4;
    };
    uint32_t mask;
} stm32_adc_config2;


typedef union{
    struct
	{
        uint32_t channel_0 : 3;
        uint32_t channel_1 : 3;
        uint32_t channel_2 : 3;
        uint32_t channel_3 : 3;
        uint32_t channel_4 : 3;
        uint32_t channel_5 : 3;
        uint32_t channel_6 : 3;
        uint32_t channel_7 : 3;
        uint32_t channel_8 : 3;
        uint32_t channel_9 : 3;
        uint32_t __reserved: 1;
        uint32_t add_one_cycle: 1;
    };
    uint32_t mask;
} stm32_adc_sample_time1;


typedef union{
    struct
	{
        uint32_t channel_10 : 3;
        uint32_t channel_11 : 3;
        uint32_t channel_12 : 3;
        uint32_t channel_13 : 3;
        uint32_t channel_14 : 3;
        uint32_t channel_15 : 3;
        uint32_t channel_16 : 3;
        uint32_t channel_17 : 3;
        uint32_t channel_18 : 3;
        uint32_t __reserved : 5;
    };
    uint32_t mask;
} stm32_adc_sample_time2;

typedef union{
    struct
	{
        uint32_t length         : 4;
        uint32_t __res1         : 2;
        uint32_t conversion_1   : 5;
        uint32_t __res2         : 1;
        uint32_t conversion_2   : 5;
        uint32_t __res3         : 1;
        uint32_t conversion_3   : 5;
        uint32_t __res4         : 1;
        uint32_t conversion_4   : 5;
        uint32_t __res5         : 3;
    };
    uint32_t mask;
} stm32_adc_regular_sequence1;


typedef union{
    struct
	{
        uint32_t conversion_5   : 5;
        uint32_t __res1         : 1;
        uint32_t conversion_6   : 5;
        uint32_t __res2         : 1;
        uint32_t conversion_7   : 5;
        uint32_t __res3         : 1;
        uint32_t conversion_8   : 5;
        uint32_t __res4         : 1;
        uint32_t conversion_9   : 5;
        uint32_t __res5         : 3;
    };
    uint32_t mask;
} stm32_adc_regular_sequence2;


typedef union{
    struct
	{
        uint32_t conversion_10  : 5;
        uint32_t __res1         : 1;
        uint32_t conversion_11  : 5;
        uint32_t __res2         : 1;
        uint32_t conversion_12  : 5;
        uint32_t __res3         : 1;
        uint32_t conversion_13  : 5;
        uint32_t __res4         : 1;
        uint32_t conversion_14  : 5;
        uint32_t __res5         : 3;
    };
    uint32_t mask;
} stm32_adc_regular_sequence3;

typedef union{
    struct
	{
        uint32_t conversion_15  : 5;
        uint32_t __res1         : 1;
        uint32_t conversion_16  : 5;
        uint32_t __res2         : 5+16;
    };
    uint32_t mask;
} stm32_adc_regular_sequence4;

typedef union{
    struct
	{
        uint32_t data : 16;
        uint32_t __reserved: 16;
    };
    uint32_t mask;
} stm32_adc_regular_data;


typedef union{
    struct
	{
        uint32_t offset         : 12;
        uint32_t __reserved     : 12;
        uint32_t offset_positive: 1;
        uint32_t saturation     : 1;
        uint32_t channel        : 5;
        uint32_t enabled        : 1;
    };
    uint32_t mask;
} stm32_adc_offset;

typedef union{
    struct
	{
        uint32_t factor_single_ended        : 7;
        uint32_t __res1                     : 9;
        uint32_t factor_differential        : 7;
        uint32_t __res2                     : 9;
    };
    uint32_t mask;
} stm32_adc_calibration;

typedef union{
    struct
	{
        uint32_t gain_compensation_coefficient     : 14;
        uint32_t __res1                     : 2+16;
    };
    uint32_t mask;
} stm32_adc_gain;

typedef struct {
    stm32_adc_interrupt_status interrupt_status;
    stm32_adc_interrupt_status interrupt_enable;
    stm32_adc_control control;
    stm32_adc_config1 config1;
    stm32_adc_config2 config2;
    stm32_adc_sample_time1 sample_time1;
    stm32_adc_sample_time2 sample_time2;
    uint32_t __res1;
    uint32_t watchdog_threshold_1;
    uint32_t watchdog_threshold_2;
    uint32_t watchdog_threshold_3;
    uint32_t __res2;
    stm32_adc_regular_sequence1 regular_sequence1;
    stm32_adc_regular_sequence2 regular_sequence2;
    stm32_adc_regular_sequence3 regular_sequence3;
    stm32_adc_regular_sequence4 regular_sequence4;
    stm32_adc_regular_data regular_data;
    uint32_t __res3[2];
    uint32_t injected_sequence;
    uint32_t __res4[4];
    stm32_adc_offset offset[4];
    uint32_t __res5[4];
    uint32_t injected_data[4];
    uint32_t __res6[4];
    uint32_t watchdog_2_config;
    uint32_t watchdog_3_config;
    uint32_t __res7[2];
    uint32_t differential_mode;
    stm32_adc_calibration calibration;
    uint32_t __res8;
    stm32_adc_gain gain_compensation;
} stm32_adc_device;

typedef union{
    struct
	{
        uint32_t master_adc_ready           : 1;
        uint32_t master_sampling_end        : 1;
        uint32_t master_conversion_end      : 1;
        uint32_t master_regular_sequence_end: 1;
        uint32_t master_overrun             : 1;
        uint32_t master_injected_conversion_end:1;
        uint32_t master_injected_sequence_end:1;
        uint32_t master_watchdog_1          : 1;
        uint32_t master_watchdog_2          : 1;
        uint32_t master_watchdog_3          : 1;
        uint32_t master_injected_overflpw   : 1;
        uint32_t __res1                     : 5;
        uint32_t slave_adc_ready            : 1;
        uint32_t slave_sampling_end         : 1;
        uint32_t slave_conversion_end       : 1;
        uint32_t slave_regular_sequence_end : 1;
        uint32_t slave_overrun              : 1;
        uint32_t slave_injected_conversion_end:1;
        uint32_t slave_injected_sequence_end: 1;
        uint32_t slave_watchdog_1           : 1;
        uint32_t slave_watchdog_2           : 1;
        uint32_t slave_watchdog_3           : 1;
        uint32_t slave_injected_overflpw    : 1;
        uint32_t __res2                     : 5;
    };
    uint32_t mask;
} stm32_adc_common_status;

typedef union{
    struct
	{
        uint32_t dual_adc_mod_select        : 5;
        uint32_t __res1                     : 3;
        uint32_t delay                      : 4;
        uint32_t __res2                     : 1;
        uint32_t circular_mode              : 1;
        uint32_t dma_access_mode_dual       : 2;
        uint32_t clock                      : 2;
        uint32_t prescale                   : 4;
        uint32_t vrefint                    : 1;
        uint32_t vts                        : 1;
        uint32_t vbat                       : 1;
        uint32_t __res3                     : 7;
    };
    uint32_t mask;
} stm32_adc_common_control;

typedef union{
    struct
	{
        uint32_t master        : 16;
        uint32_t slave         : 16;
    };
    uint32_t mask;
} stm32_adc_common_dual_data;

typedef struct{
    stm32_adc_common_status status;
    stm32_adc_common_control control;
    stm32_adc_common_dual_data data;
}stm32_adc_common;


// EOSMP signifies end of sampling
//"Bulb" mode looks interesting
// Continuous conversion mode (CONT = 1) in order 






static volatile stm32_adc_device *const stm32_adc_1 = (volatile stm32_adc_device *) 0x50000000; 
static volatile stm32_adc_device *const stm32_adc_2 = (volatile stm32_adc_device *) 0x50000100; 
static volatile stm32_adc_common *const stm32_adc_common_12 = (volatile stm32_adc_common *) 0x50000300; 

static volatile stm32_adc_device *const stm32_adc_3 = (volatile stm32_adc_device *) 0x50000400; 
static volatile stm32_adc_device *const stm32_adc_4 = (volatile stm32_adc_device *) 0x50000500; 
static volatile stm32_adc_device *const stm32_adc_5 = (volatile stm32_adc_device *) 0x50000600; 
static volatile stm32_adc_common *const stm32_adc_common_345 = (volatile stm32_adc_common *) 0x50000700; 

#endif