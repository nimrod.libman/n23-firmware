#ifndef SPI_H
#define SPI_H
typedef union {
  struct {
    uint32_t rxne : 1; // Recv buffer not empty
    uint32_t txe  : 1; // Transmit buffer emptied
    uint32_t unused:2; // channel side, underrun (unused for SPI)
    uint32_t crcerr:1; // CRC error
    uint32_t modf : 1; // Mode fault
    uint32_t ovr  : 1; // Overrun 
    uint32_t bsy  : 1; // busy
    uint32_t fre  : 1; // frame format error (Not in Master mode!)
    uint32_t frlvl: 2; // FIFO fullness (Not in Master mode?) (Only the Rx buffer?)
    uint32_t ftlvl: 2; // FIFO fullness (Tx buffer?)
    uint32_t reserved: 3; // Unused
    uint32_t dummy: 16;
  };
  struct {
    uint32_t mask;
  };
} stm32_spi_status;

typedef union
{
	struct
	{
		uint32_t cpha     :  1;
		uint32_t cpol     :  1;
		uint32_t mstr     :  1;
		uint32_t br       :  3;
		uint32_t spe      :  1;
		uint32_t lsbfirst :  1;
		uint32_t ssi      :  1;
		uint32_t ssm      :  1;
		uint32_t rxonly   :  1;
		uint32_t crcl     :  1;
		uint32_t crcnext  :  1;
		uint32_t crcen    :  1;
		uint32_t bidioe   :  1;
		uint32_t bidimode :  1;
		uint32_t unused   : 16;
	};

	uint32_t mask;
} stm32_spi_c1;

typedef union
{
	struct
	{
		uint32_t rxdmaen :  1;
		uint32_t txdmaen :  1;
		uint32_t ssoe    :  1;
		uint32_t nssp    :  1;
		uint32_t frf     :  1;
		uint32_t errie   :  1;
		uint32_t rxneie  :  1;
		uint32_t txeie   :  1;
		uint32_t ds      :  4;
		uint32_t frxth   :  1;
		uint32_t ldma_rx :  1;
		uint32_t ldma_tx :  1;
		uint32_t unused  : 17;
	};

	uint32_t mask;
} stm32_spi_c2;

typedef struct{
  
    stm32_spi_c1 c1;   
    stm32_spi_c2 c2;
    stm32_spi_status s;
    union {
		uint32_t d32;
		uint16_t d16;
		uint8_t  d8;
	}; 
    uint32_t crcp;
    uint32_t rxcrc;
    uint32_t txcrc;
    //Two I2S registers come after trhgis but we don't care


} stm32_spi;



static volatile stm32_spi *const stm32_spi1 = (volatile stm32_spi *)0x40013000; 
static volatile stm32_spi *const stm32_spi2 = (volatile stm32_spi *)0x40003800; 
static volatile stm32_spi *const stm32_spi3 = (volatile stm32_spi *)0x40003C00;
//static volatile stm32_spi *const stm32_spi4 = (volatile stm32_spi *)0x40013C00; // Not on the G431
//SPI1/2/3 are all AF5. 2/3 can be AF6

#endif
