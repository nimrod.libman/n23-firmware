#ifndef DMA_H
#define DMA_H

#include <stdint.h>
typedef union {
	struct
	{
		uint32_t en      : 1; // Enable channel
        uint32_t tcie    : 1; // Enable transfer complete interrupt
        uint32_t htie    : 1; // Enable half transfer interrupt
        uint32_t teie    : 1; // Enable transfer error interrupt
        uint32_t dir     : 1; // Transfer direction (0: Peripheral-to-memory, 1: Memory-to-peripheral)
        uint32_t circ    : 1; // Circular mode
        uint32_t pinc    : 1; // Peripheral increment mode
        uint32_t minc    : 1; // Memory incrmement mode
        uint32_t psize   : 2; // Size of each individual transfer to peripheral
        uint32_t msize   : 2; // Size of each individual transfer (0:8 bits, 1: 16 bit, 2: 32 bits) to memory
        uint32_t pl      : 2; // Priority level (0 lowest, 3 highest)
        uint32_t mem2mem : 1; // Memory-to-memeory transfer )rather than involving a peripheral
		uint32_t unused  : 17;
	};

	uint32_t mask;
} stm32_dma_device_config;


typedef struct {
    stm32_dma_device_config config; 
    uint32_t ndt; // Number of data to transfer  (Actually 16 bit)
    void* peripheral_address;
    void* memory_address;
    uint32_t __reserved;
} stm32_dma_device_channel;

typedef struct {
    uint32_t is; // interrupt status
    uint32_t ifc; //interrupt flag clear    
    stm32_dma_device_channel chan[8];
} stm32_dma_device;





/* From the docs:

The following sequence is needed to configure a DMA channel x:
1.Set the peripheral register address in the DMA_CPARx register.
The data is moved from/to this address to/from the memory after the peripheral event,
or after the channel is enabled in memory-to-memory mode.
2.Set the memory address in the DMA_CMARx register.
The data is written to/read from the memory after the peripheral event or after the
channel is enabled in memory-to-memory mode.
3.Configure the total number of data to transfer in the DMA_CNDTRx register.
After each data transfer, this value is decremented.
4.Configure the parameters listed below in the DMA_CCRx register:
5.
–the channel priority
–the data transfer direction
–the circular mode
–the peripheral and memory incremented mode
–the peripheral and memory data size
–the interrupt enable at half and/or full transfer and/or transfer error
Activate the channel by setting the EN bit in the DMA_CCRx register.
A channel, as soon as enabled, may serve any DMA request from the peripheral connected
to this channel, or may start a memory-to-memory block transfer.


*/


static volatile stm32_dma_device *const stm32_dma1 = (volatile stm32_dma_device *)0x40020000; 
static volatile stm32_dma_device *const stm32_dma2 = (volatile stm32_dma_device *)0x40020400; 


typedef enum {
    //TODO fill in resources: table 91 in reference manual
    STM32_DAC1_CH1 = 6,
    STM32_DAC1_CH2 = 7,
    STM32_TIM6_UP  = 8,
    STM32_TIM7_UP  = 9,
    STM32_SPI1_RX  = 10,
    STM32_SPI1_TX  = 11,
    STM32_SPI2_RX  = 12,
    STM32_SPI2_TX  = 13,
    STM32_SPI3_RX  = 14,
    STM32_SPI3_TX  = 15,
} stm32_dmamux_resource;

typedef union {
	struct
	{
		uint32_t dmareq_id : 7; // Request ID
        uint32_t __res1    : 1;
        uint32_t soie      : 1; //Sycn overrun interrupt enable
        uint32_t ege       : 1; // Event generation enable
        uint32_t __res2    : 6; 
        uint32_t se        : 1; // Sync enable
        uint32_t spol      : 2; // Sync polarity
        uint32_t nbreq     : 5; // # DMA requests, -1 , to foreward after sync event
        uint32_t sync_id   : 5; //TODO get an enum for values?
		uint32_t __unused  : 3;
	};

	uint32_t mask;
} stm32_dmamux_request_channel_config;

typedef union {
	struct
	{
		uint32_t sig_id    : 5; // TODO I don't get this at all
        uint32_t __res1    : 3;
        uint32_t oie       : 1; //Trigger overrun interrupt enable
        uint32_t __res2    : 7; 
        uint32_t ge        : 1; // request generator enable enable
        uint32_t gpol      : 2; // Request generator trigger polarity
        uint32_t gnbreq    : 5; // # DMA requests, -1 , to foreward after trigger event
		uint32_t __unused  : 8;
	};

	uint32_t mask;
} stm32_dmamux_request_generator_config;

typedef struct{
    stm32_dmamux_request_channel_config cc[16];  
    uint32_t __reserved1[16];
    uint32_t cs;           // interrupt channel status
    uint32_t cf;           // interrupt clear flag
    uint32_t __reserved2[30];
    stm32_dmamux_request_generator_config rgc[4];
    uint32_t __reserved3[12];
    uint32_t rgs;
    uint32_t rgcf;
} stm32_dma_mux_device;


static volatile stm32_dma_mux_device *const stm32_dmamux_device = (volatile stm32_dma_mux_device *)0x40020800; 


#endif
