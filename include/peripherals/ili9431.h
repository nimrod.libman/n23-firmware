#ifndef ILI9431_H
#define ILI9431_H

// Requires timers to have been set up
void display_set_up(const uint32_t desired_spi_clock);

void display_send_column_addr(uint16_t min,uint16_t max);
void display_send_page_addr(uint16_t min,uint16_t max);
void display_send_memory_write();
void display_select(bool select);
void display_send_pixel(uint16_t pix);


#endif