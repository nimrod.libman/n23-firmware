#include <stdint.h>
#include "hw/stm32g4.h"
#include "hw/timers/general.h"
#include "hw/timers/basic.h"
#include "lib/n23.h"


#define N23_TIMERS_TICKER stm32_tim2
#define N23_TIMERS_DELAY stm32_tim6 
#define N23_TIMERS_DELAY_INTERRUPT stm32_g4_interrupt_tim6_dacunder
// TODO use one of the shorter timers for delays, rather than the full 32 bit ones. What, do I expect to delay more than 65K us?
// TODO figure out the relation between AHB and core clock..
void n23_timers_init(){
    stm32_clock_peripheral(stm32_peripheral_enable_TIM2, true); 
    stm32_clock_peripheral(stm32_peripheral_enable_TIM6, true); 

    N23_TIMERS_DELAY->config1.update_disable = false;
    N23_TIMERS_DELAY->config1.update_source = 0b1;
    N23_TIMERS_DELAY->config1.one_pulse_mode = true;
    N23_TIMERS_DELAY->config1.auto_reload = true;
    N23_TIMERS_DELAY->config1.uif_remap = false;
    N23_TIMERS_DELAY->config1.dither = false;

    N23_TIMERS_DELAY->interrupts_enable.update = true;
    cortex_m4_nvic_enable_interrupt(N23_TIMERS_DELAY_INTERRUPT);
}

// TODO prescaling?
void n23_timers_start_ticker(){
    N23_TIMERS_TICKER->config1.update_disable = false;
    N23_TIMERS_TICKER->config1.direction = 0b1; // Count Down. If we were counting up then update events would reset the prescaler (????)
    N23_TIMERS_TICKER->config1.update_source = 0b1;
    N23_TIMERS_TICKER->config1.one_pulse_mode = false;
    N23_TIMERS_TICKER->config1.center_align_mode = 0b00; // Do not alternate directions
    N23_TIMERS_TICKER->config1.auto_reload = true;
    N23_TIMERS_TICKER->config1.clock_division = 0b00;
    N23_TIMERS_TICKER->config1.uif_remap = false;
    N23_TIMERS_TICKER->config1.dither = false;

    N23_TIMERS_TICKER->prescaler.prescaler = 0;
    N23_TIMERS_TICKER->auto_reload = UINT32_MAX;

    N23_TIMERS_TICKER->config1.enable = true;
}


uint32_t n23_timers_ticker(){
    return N23_TIMERS_TICKER->counter.mask;
}


void n23_timers_sleep(uint16_t microseconds){
    N23_TIMERS_DELAY->prescaler.prescaler = n23_get_core_clock_mhz(); 
    N23_TIMERS_DELAY->auto_reload.auto_reload = microseconds; 
    // Generate an update event to start the timer countdown.
    N23_TIMERS_DELAY->generate_event.update = true;
     N23_TIMERS_DELAY->interrupt_status.mask = 0;
    // I shit you not, the update event has to be set ebefore enabling the timer
    // When these two lines are were the other way around, this never worked.
    N23_TIMERS_DELAY->config1.enable = true; 
    while(N23_TIMERS_DELAY->config1.enable) {
        asm volatile("wfi");
    }
}

void arm_vector_irq_tim6_dacunder(){
    N23_TIMERS_DELAY->interrupt_status.update = false; // So we don't just re-enter the interrupt
}