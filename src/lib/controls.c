#include "lib/controls.h"
#include "hw/adc.h"
#include "hw/gpio.h"
#include "lib/timers.h"
#include <stdbool.h>

#ifdef N23_G431_BOARD_COMPAT

#define STM32_INPUT_SELECT_LEFT_BUTTONS (volatile stm32_gpio*)STM32_GPIOB, 4
#define STM32_INPUT_SELECT_RIGHT_BUTTONS (volatile stm32_gpio*)STM32_GPIOB, 5
#define STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT (volatile stm32_gpio*)STM32_GPIOB, 6
#define STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS (volatile stm32_gpio*)STM32_GPIOB, 7

#define STM32_INPUT_DETECT_LR_RR_RT_SELECT (volatile stm32_gpio*)STM32_GPIOB, 8
#define STM32_INPUT_DETECT_LL_RL_RS_START (volatile stm32_gpio*)STM32_GPIOB, 9
#define STM32_INPUT_DETECT_LD_RD_LT_RJ (volatile stm32_gpio*)STM32_GPIOA, 15
#define STM32_INPUT_DETECT_LU_RU_LS_LJ (volatile stm32_gpio*)STM32_GPIOC, 13

void n23_input_buttons_init(void)
{
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOA, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOB, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOC, true);

    stm32_gpio_mode_set(STM32_INPUT_SELECT_LEFT_BUTTONS, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_INPUT_SELECT_LEFT_BUTTONS, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SELECT_RIGHT_BUTTONS, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_INPUT_SELECT_RIGHT_BUTTONS, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, stm32_pin_speed_VeryHigh);

    stm32_gpio_mode_set(STM32_INPUT_DETECT_LR_RR_RT_SELECT, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_DETECT_LR_RR_RT_SELECT, stm32_pin_pull_Down);
    stm32_gpio_mode_set(STM32_INPUT_DETECT_LL_RL_RS_START, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_DETECT_LL_RL_RS_START, stm32_pin_pull_Down);
    stm32_gpio_mode_set(STM32_INPUT_DETECT_LD_RD_LT_RJ, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_DETECT_LD_RD_LT_RJ, stm32_pin_pull_Down);
    stm32_gpio_mode_set(STM32_INPUT_DETECT_LU_RU_LS_LJ, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_DETECT_LU_RU_LS_LJ, stm32_pin_pull_Down);
}

void n23_input_joy_init(void)
{
    // JOY_L_Y is PB10 (... doesn't seem to be connected to a ADC pin)
    // JOY_L_X is PB11 (ADC12_IN14)
    // JOY_R_Y is PA0 (ADC12_IN1)
    // JOY_R_X is PA1 (ADC12_IN2)

    stm32_rcc->independent_clock_config.adc12 = 0b10; // system clock
    stm32_clock_peripheral(stm32_peripheral_enable_ADC12, true);
    stm32_adc_common_12->control.clock = 0b10; // Use AHB clock for the ADC, scaled to 2. (unscaled requires a prescaled RCC)

    // Calibrate the ADC
    stm32_adc_1->control.deep_power_down = false;
    stm32_adc_1->control.voltage_regulator = true;
    n23_timers_sleep(20);

    stm32_adc_1->control.differential_mode = false;

    stm32_adc_1->control.calibration = true;
    while (stm32_adc_1->control.calibration) { };

    stm32_adc_1->config1.resolution = 0b10; // 8 bit resolution
    stm32_adc_1->sample_time1.channel_1 = 0b101; // Big value for safety reasons; couldnt figure out what the optimal value is
    stm32_adc_1->sample_time1.channel_2 = 0b101;

    stm32_adc_common_12->control.vbat = true;
    stm32_adc_common_12->control.vts = true;
    stm32_adc_common_12->control.vrefint = true;

    stm32_adc_1->control.enable = true;
    while (!stm32_adc_1->interrupt_status.ready) { };
}

bool n23_input_button_detect_select(volatile stm32_gpio* select_port, uint8_t select_index, volatile stm32_gpio* detect_port, uint8_t detect_index)
{
    // TODO.. clear the other pins with one assignment...

    stm32_gpio_out_set(STM32_INPUT_SELECT_LEFT_BUTTONS, false);
    stm32_gpio_out_set(STM32_INPUT_SELECT_RIGHT_BUTTONS, false);
    stm32_gpio_out_set(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, false);
    stm32_gpio_out_set(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, false);

    stm32_gpio_out_set(select_port, select_index, true);

    n23_timers_sleep(1); // Wait for the lines to absolutely be pulled high

    return stm32_gpio_in_get(detect_port, detect_index);
}

bool n23_input_left_l(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_BUTTONS, STM32_INPUT_DETECT_LL_RL_RS_START); }
bool n23_input_left_u(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_BUTTONS, STM32_INPUT_DETECT_LU_RU_LS_LJ); }
bool n23_input_left_r(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_BUTTONS, STM32_INPUT_DETECT_LR_RR_RT_SELECT); }
bool n23_input_left_d(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_BUTTONS, STM32_INPUT_DETECT_LD_RD_LT_RJ); }

bool n23_input_right_l(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_BUTTONS, STM32_INPUT_DETECT_LL_RL_RS_START); }
bool n23_input_right_u(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_BUTTONS, STM32_INPUT_DETECT_LU_RU_LS_LJ); }
bool n23_input_right_r(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_BUTTONS, STM32_INPUT_DETECT_LR_RR_RT_SELECT); }
bool n23_input_right_d(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_BUTTONS, STM32_INPUT_DETECT_LD_RD_LT_RJ); }

bool n23_input_right_shoulder_inner(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, STM32_INPUT_DETECT_LL_RL_RS_START); }
bool n23_input_right_shoulder_outer(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, STM32_INPUT_DETECT_LR_RR_RT_SELECT); }
bool n23_input_left_shoulder_inner(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, STM32_INPUT_DETECT_LU_RU_LS_LJ); }
bool n23_input_left_shoulder_outer(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, STM32_INPUT_DETECT_LD_RD_LT_RJ); }

bool n23_input_left_joystick_button(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, STM32_INPUT_DETECT_LU_RU_LS_LJ); }
bool n23_input_right_joystick_button(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_RIGHT_SHOULDER_JOYSTICKS, STM32_INPUT_DETECT_LD_RD_LT_RJ); }
bool n23_input_left_select(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, STM32_INPUT_DETECT_LL_RL_RS_START); }
bool n23_input_right_select(void) { return n23_input_button_detect_select(STM32_INPUT_SELECT_LEFT_SHOULDER_START_SELECT, STM32_INPUT_DETECT_LR_RR_RT_SELECT); }

n23_input_joy_position n23_input_right_joy(void)
{
    // JOY_R_Y is PA0 (ADC12_IN1)
    // JOY_R_X is PA1 (ADC12_IN2)

    // TODO Look into using the interrupt register to read the first bit, thus allowing us to use the queuing
    stm32_adc_1->regular_sequence1.length = 0; // 1 conversion
    stm32_adc_1->regular_sequence1.conversion_1 = 2;

    stm32_adc_1->control.start_regular = true;
    while (stm32_adc_1->control.start_regular) { };

    uint8_t x = stm32_adc_1->regular_data.data;

    stm32_adc_1->regular_sequence1.length = 0; // 1 conversion
    stm32_adc_1->regular_sequence1.conversion_1 = 1;
    stm32_adc_1->control.start_regular = true;
    while (stm32_adc_1->control.start_regular) { };

    uint8_t y = stm32_adc_1->regular_data.data;
    n23_input_joy_position r = { x = x, y = UINT16_MAX - y };
    return r;
}

n23_input_joy_position n23_input_left_joy(void)
{
    n23_input_joy_position r = { x = 128, y = 128 };
    return r;
}

#else

#define STM32_INPUT_LEFT_U (volatile stm32_gpio*)STM32_GPIOC, 4
#define STM32_INPUT_LEFT_D (volatile stm32_gpio*)STM32_GPIOA, 10
#define STM32_INPUT_LEFT_L (volatile stm32_gpio*)STM32_GPIOA, 9
#define STM32_INPUT_LEFT_R (volatile stm32_gpio*)STM32_GPIOB, 7
#define STM32_INPUT_RIGHT_U (volatile stm32_gpio*)STM32_GPIOC, 14
#define STM32_INPUT_RIGHT_D (volatile stm32_gpio*)STM32_GPIOB, 8
#define STM32_INPUT_RIGHT_L (volatile stm32_gpio*)STM32_GPIOB, 9
#define STM32_INPUT_RIGHT_R (volatile stm32_gpio*)STM32_GPIOC, 5
#define STM32_INPUT_SHOULDER_LEFT_I (volatile stm32_gpio*)STM32_GPIOB, 6
#define STM32_INPUT_SHOULDER_LEFT_O (volatile stm32_gpio*)STM32_GPIOA, 8
#define STM32_INPUT_SHOULDER_RIGT_I (volatile stm32_gpio*)STM32_GPIOB, 4
#define STM32_INPUT_SHOULDER_RIGT_O (volatile stm32_gpio*)STM32_GPIOC, 13
#define STM32_INPUT_START (volatile stm32_gpio*)STM32_GPIOA, 7
#define STM32_INPUT_SELCT (volatile stm32_gpio*)STM32_GPIOA, 6
#define STM32_INPUT_JOY_BUTTON_L (volatile stm32_gpio*)STM32_GPIOB, 2
#define STM32_INPUT_JOY_BUTTON_R (volatile stm32_gpio*)STM32_GPIOC, 8

void n23_input_buttons_init(void)
{

    stm32_clock_peripheral(stm32_peripheral_enable_GPIOA, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOB, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOC, true);
    // Given all of the overlap, this could probably transformed into like... 9 writes.
    stm32_gpio_mode_set(STM32_INPUT_LEFT_U, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_LEFT_U, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_LEFT_U, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_LEFT_D, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_LEFT_D, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_LEFT_D, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_LEFT_L, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_LEFT_L, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_LEFT_L, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_LEFT_R, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_LEFT_R, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_LEFT_R, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_RIGHT_U, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_RIGHT_U, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_RIGHT_U, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_RIGHT_D, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_RIGHT_D, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_RIGHT_D, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_RIGHT_L, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_RIGHT_L, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_RIGHT_L, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_RIGHT_R, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_RIGHT_R, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_RIGHT_R, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SHOULDER_LEFT_I, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_SHOULDER_LEFT_I, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_SHOULDER_LEFT_I, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SHOULDER_LEFT_O, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_SHOULDER_LEFT_O, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_SHOULDER_LEFT_O, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SHOULDER_RIGT_I, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_SHOULDER_RIGT_I, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_SHOULDER_RIGT_I, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SHOULDER_RIGT_O, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_SHOULDER_RIGT_O, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_SHOULDER_RIGT_O, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_START, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_START, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_START, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_SELCT, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_SELCT, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_SELCT, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_JOY_BUTTON_L, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_JOY_BUTTON_L, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_JOY_BUTTON_L, stm32_pin_speed_VeryHigh);
    stm32_gpio_mode_set(STM32_INPUT_JOY_BUTTON_R, stm32_pin_mode_Input);
    stm32_gpio_pupd_set(STM32_INPUT_JOY_BUTTON_R, stm32_pin_pull_Up);
    stm32_gpio_speed_set(STM32_INPUT_JOY_BUTTON_R, stm32_pin_speed_VeryHigh);
}

bool n23_input_left_l(void) { return !stm32_gpio_in_get(STM32_INPUT_LEFT_L); }
bool n23_input_left_u(void) { return !stm32_gpio_in_get(STM32_INPUT_LEFT_U); }
bool n23_input_left_r(void) { return !stm32_gpio_in_get(STM32_INPUT_LEFT_R); }
bool n23_input_left_d(void) { return !stm32_gpio_in_get(STM32_INPUT_LEFT_D); }

bool n23_input_right_l(void) { return !stm32_gpio_in_get(STM32_INPUT_RIGHT_L); }
bool n23_input_right_u(void) { return !stm32_gpio_in_get(STM32_INPUT_RIGHT_U); }
bool n23_input_right_r(void) { return !stm32_gpio_in_get(STM32_INPUT_RIGHT_R); }
bool n23_input_right_d(void) { return !stm32_gpio_in_get(STM32_INPUT_RIGHT_D); }

bool n23_input_left_shoulder_inner(void) { return !stm32_gpio_in_get(STM32_INPUT_SHOULDER_LEFT_I); }
bool n23_input_left_shoulder_outer(void) { return !stm32_gpio_in_get(STM32_INPUT_SHOULDER_LEFT_O); }
bool n23_input_right_shoulder_inner(void) { return !stm32_gpio_in_get(STM32_INPUT_SHOULDER_RIGT_I); }
bool n23_input_right_shoulder_outer(void) { return !stm32_gpio_in_get(STM32_INPUT_SHOULDER_RIGT_O); }

bool n23_input_left_joystick_button(void) { return !stm32_gpio_in_get(STM32_INPUT_JOY_BUTTON_L); }
bool n23_input_right_joystick_button(void) { return !stm32_gpio_in_get(STM32_INPUT_JOY_BUTTON_R); }
bool n23_input_left_select(void) { return !stm32_gpio_in_get(STM32_INPUT_START); }
bool n23_input_right_select(void) { return !stm32_gpio_in_get(STM32_INPUT_SELCT); }

#define STM32_INPUT_JOY_L_X (volatile stm32_gpio*)STM32_GPIOB, 0
#define STM32_INPUT_JOY_L_Y (volatile stm32_gpio*)STM32_GPIOB, 1
#define STM32_INPUT_JOY_R_X (volatile stm32_gpio*)STM32_GPIOC, 2
#define STM32_INPUT_JOY_R_Y (volatile stm32_gpio*)STM32_GPIOC, 1

void set_up_adc12(void)
{
    if (stm32_adc_1->control.enable) {
        return;
    }
    stm32_clock_peripheral(stm32_peripheral_enable_ADC12, true);
    stm32_adc_common_12->control.clock = 0b10; // Use AHB clock for the ADC, scaled to 2. (unscaled requires a prescaled RCC)

    // Calibrate the ADC
    stm32_adc_control control = stm32_adc_1->control;
    control.deep_power_down = false;
    control.voltage_regulator = true;
    stm32_adc_1->control = control;

    n23_timers_sleep(20);

    control.differential_mode = false;
    stm32_adc_1->control = control;
    control.calibration = true;
    stm32_adc_1->control = control;

    while (stm32_adc_1->control.calibration) { };

    stm32_adc_1->config1.resolution = 0b00; // 12 bit resolution
    stm32_adc_1->sample_time1.channel_1 = 0b101; // Big value for safety reasons; couldnt figure out what the optimal value is
    stm32_adc_1->sample_time1.channel_2 = 0b101;

    // set up oversampling to 16 bits
    stm32_adc_config2 config2 = stm32_adc_1->config2;
    config2.regular_oversampling_enable = true;
    config2.oversampling_ratio = 0b111;
    config2.oversampling_shift = 4;

    stm32_adc_1->config2 = config2;

    stm32_adc_common_control common_control = stm32_adc_common_12->control;
    common_control.vbat = true;
    common_control.vts = true;
    common_control.vrefint = true;
    stm32_adc_common_12->control = common_control;

    stm32_adc_1->control.enable = true;
    while (!stm32_adc_1->interrupt_status.ready) { };
}

void n23_input_joy_init(void)
{
    // JOY_L_Y is PB1 (ADC1_IN12/ADC3_IN1)
    // JOY_L_X is PB0 (ADC12_IN15/ADC3_IN12) (This may be a UCPD pin? FRSTX?)
    // JOY_R_Y is PC1 (ADC12_IN7)
    // JOY_R_X is PC2 (ADC12_IN8)

    stm32_gpio_mode_set(STM32_INPUT_JOY_L_X, stm32_pin_mode_Analog);
    stm32_gpio_mode_set(STM32_INPUT_JOY_L_Y, stm32_pin_mode_Analog);
    stm32_gpio_mode_set(STM32_INPUT_JOY_R_X, stm32_pin_mode_Analog);
    stm32_gpio_mode_set(STM32_INPUT_JOY_R_Y, stm32_pin_mode_Analog);

    stm32_rcc->independent_clock_config.adc12 = 0b10; // system clock
    set_up_adc12();
}

// TODO calibration, it seems these joysticks don't just go 0-255.
n23_input_joy_position read_joystick(uint8_t x_channel, uint8_t y_channel)
{
    // TODO Look into using the interrupt register to read the first bit, thus allowing us to use the queuing
    stm32_adc_1->regular_sequence1.length = 0; // 1 conversion
    stm32_adc_1->regular_sequence1.conversion_1 = x_channel;

    stm32_adc_1->control.start_regular = true;
    while (stm32_adc_1->control.start_regular) { };

    uint16_t x = stm32_adc_1->regular_data.data;

    stm32_adc_1->regular_sequence1.length = 0; // 1 conversion
    stm32_adc_1->regular_sequence1.conversion_1 = y_channel;
    stm32_adc_1->control.start_regular = true;
    while (stm32_adc_1->control.start_regular) { };

    uint16_t y = stm32_adc_1->regular_data.data;
    n23_input_joy_position r = { x = x, y = y };
    return r;
}

n23_input_joy_position n23_input_right_joy(void)
{
    // JOY_R_Y is PC1 (ADC12_IN7)
    // JOY_R_X is PC2 (ADC12_IN8)
    // TODO read calibration factors taken from firmware
    return read_joystick(8, 7);
}

n23_input_joy_position n23_input_left_joy(void)
{
    // JOY_L_Y is PB1 (ADC1_IN12/ADC3_IN1)
    // JOY_L_X is PB0 (ADC12_IN15/ADC3_IN12)

    // TODO read calibration factors taken from firmware
    return read_joystick(15, 12);
}

#endif
