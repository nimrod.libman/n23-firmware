#include "hw/dma.h"
#include "hw/spi.h"
#include "hw/stm32g4.h"
#include "peripherals/ili9431.h"
#include "lib/displaylib.h"
#include <stdint.h>
#include <stdbool.h>



#define SCREEN_DMA_CHANNEL 2
#define SCREEN_DMA_CHANNEL_INTERRUPT stm32_g4_interrupt_dma1_ch3

#ifdef N23_G431_BOARD_COMPAT
#define SCREEN_DMA_REQUEST_ID STM32_SPI1_TX
#define STM32_DISPLAY_BUS stm32_spi1
#else
#define SCREEN_DMA_REQUEST_ID STM32_SPI2_TX
#define STM32_DISPLAY_BUS stm32_spi2
#endif

typedef struct{
    uint16_t linebuffers[2][DISPLAY_COLUMNS];
    uint16_t* current_linebuffer;
    uint16_t current_line;
} n23_displaylib_linebuffers;

n23_line_renderer n23_display_current_renderer;
n23_displaylib_linebuffers n23_display_linebuffers;

void n23_displaylib_init(uint32_t desired_spi_clock){

    display_set_up(desired_spi_clock);
    display_select(true);
    display_send_column_addr(0,DISPLAY_COLUMNS-1);
    display_send_page_addr(0,DISPLAY_ROWS-1);
    display_send_memory_write();
    display_select(false);

    stm32_clock_peripheral(stm32_peripheral_enable_DMA1, true);
    stm32_clock_peripheral(stm32_peripheral_enable_DMA1MUX, true);


    n23_display_linebuffers.current_line = 0;
    // Set up DMA for the display's SPI
    // Update SPI registers to use interrupts (we will use the later to call the render callbacks)
    STM32_DISPLAY_BUS->c1.spe = false;
    stm32_spi_c2 c2 = {.mask = STM32_DISPLAY_BUS->c2.mask};
    c2.txeie = true; // Enable an interrupt when finished transmission
    c2.txdmaen = true;  
    // Won't enable error interrupts, we'll just power through those like a boss
    STM32_DISPLAY_BUS->c2 = c2;

    //Point a DMA device at the SPI device
    stm32_dma_device_channel dma_channel = stm32_dma1->chan[SCREEN_DMA_CHANNEL];
    dma_channel.config.tcie = true; // Enable the "transfer complete" interrupt
    dma_channel.config.htie = false; // Disable half-transfter interrupt
    dma_channel.config.teie = false; // Disable error interrupt
    dma_channel.config.dir  = true; // Read from memory into peripheral
    dma_channel.config.circ = false;
    dma_channel.config.pinc = false;
    dma_channel.config.minc = true; // Increment Memory address to read from
    dma_channel.config.psize = 0b01;    
    dma_channel.config.msize = 0b01; 
    dma_channel.config.pl = 0b10; // Second highest priority (second only to audio)
    dma_channel.config.mem2mem = false;
    dma_channel.peripheral_address = (uint16_t*)&(STM32_DISPLAY_BUS->d16);
    stm32_dma1->chan[SCREEN_DMA_CHANNEL] = dma_channel;

    
    
    stm32_dmamux_request_channel_config cc = stm32_dmamux_device->cc[SCREEN_DMA_CHANNEL];
    cc.dmareq_id = SCREEN_DMA_REQUEST_ID;
    stm32_dmamux_device->cc[SCREEN_DMA_CHANNEL] = cc;

    // TODO: should this be done in `start`, rather than `init`?
    // activate NVIC so we can get DMA1 ch3 interrupts when DMA finishes writing line
    cortex_m4_nvic_enable_interrupt(SCREEN_DMA_CHANNEL_INTERRUPT);

}


void start_linebuffer_dma_transaction(void){
    stm32_dma_device_channel chan = stm32_dma1->chan[SCREEN_DMA_CHANNEL];
    chan.config.en = false;
    stm32_dma1->chan[SCREEN_DMA_CHANNEL] = chan;

    stm32_dma1->ifc = 0b1111 << (4*SCREEN_DMA_CHANNEL); // Clear all interrupt flags for the DMA (Yes this could be nicer, idc for now)

    chan.memory_address = n23_display_linebuffers.current_linebuffer;
    chan.ndt = DISPLAY_COLUMNS;
    stm32_dma1->chan[SCREEN_DMA_CHANNEL] = chan;
    chan.config.en = true;
    stm32_dma1->chan[SCREEN_DMA_CHANNEL] = chan;
}

// Start running the render loop. 
void n23_displaylib_start(n23_line_renderer initial_renderer){
    display_select(true);
    n23_displaylib_set_renderer(initial_renderer);
    STM32_DISPLAY_BUS->c1.spe = true;

    // Pre-fill the linebuffers
    initial_renderer(n23_display_linebuffers.linebuffers[0],0);
    initial_renderer(n23_display_linebuffers.linebuffers[1],1);
    // Start the DMA writing
    n23_display_linebuffers.current_line = 0; 
    n23_display_linebuffers.current_linebuffer = n23_display_linebuffers.linebuffers[0];
    start_linebuffer_dma_transaction();

    n23_display_linebuffers.current_line = 1; 
    n23_display_linebuffers.current_linebuffer = n23_display_linebuffers.linebuffers[1];
}


void n23_displaylib_set_renderer(n23_line_renderer new_renderer){
    n23_display_current_renderer = new_renderer;
}

void arm_vector_irq_dma1_ch3(void){    
    // A display line write operation just ended. Start writing the next line to display, and make ready a line to follow.

    // Start DMAing the new (full) buffer 
    start_linebuffer_dma_transaction();

    // swap buffers so we write to the empty buffer
    if (n23_display_linebuffers.current_linebuffer == n23_display_linebuffers.linebuffers[0]){
        n23_display_linebuffers.current_linebuffer = n23_display_linebuffers.linebuffers[1];
    }else{
        n23_display_linebuffers.current_linebuffer = n23_display_linebuffers.linebuffers[0];
    }    

    n23_display_linebuffers.current_line = (n23_display_linebuffers.current_line +1) % DISPLAY_ROWS;

    // call the render callback to fill the now-used-up buffer so it can be full
    n23_display_current_renderer(n23_display_linebuffers.current_linebuffer, n23_display_linebuffers.current_line);
}
