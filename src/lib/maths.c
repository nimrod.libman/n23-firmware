
#include "hw/cordic.h"
#include "hw/stm32g4.h"


void n23_cordic_math_init(void){
    stm32_clock_peripheral(stm32_peripheral_enable_CORDIC, true);
    stm32_cordic_control_status c = stm32_cordic->control;
    c.precision = 3;
    c.arg_size = 0;
    c.result_size = 0; // 1q31 inputs and outputs
    c.scale = 0; // scaled by a factor of 1
    c.nargs = 1; // Most CORDIC operations are binary. We can write a second arg of dummy data to trigger the unary ones. 
    c.nres = 0;  // We will only care about the first output datapoint, we don't need to use the second output. Setting this means we don't need to read it to clear the solver
    stm32_cordic->control = c; // Why do it this way? Well the changes dont stick otherwise
}

const uint32_t Q131_APPROX_1 = 0x7FFFFFFF;

static inline uint32_t binary_op(stm32_cordic_functions op, uint32_t a, uint32_t b){
    stm32_cordic_control_status c = stm32_cordic->control;
    c.func = op;
    stm32_cordic->control = c; // For some reason I can't just poke the func register directly, it crashes the device
    stm32_cordic->arguments = a; 
    stm32_cordic->arguments = b;
    asm volatile("nop");
    asm volatile("nop");
    asm volatile("nop");
    return stm32_cordic->results;
}

static inline uint32_t unary_op(stm32_cordic_functions op, uint32_t x){
    return binary_op(op,x,0);
}

uint32_t n23_cordic_sin(uint32_t rads_div_pi){
    return binary_op(STM32_CORDIC_SIN, rads_div_pi, Q131_APPROX_1);
}

uint32_t n23_cordic_cos(uint32_t rads_div_pi){
    return binary_op(STM32_CORDIC_COS, rads_div_pi, Q131_APPROX_1);
}

uint32_t n23_cordic_sinh(uint32_t x){
    return unary_op(STM32_CORDIC_SINH,x);
}
uint32_t n23_cordic_cosh(uint32_t x){
    return unary_op(STM32_CORDIC_COSH,x);
}

uint32_t n23_cordic_cartesian_to_angle(uint32_t x, uint32_t y){
    return binary_op(STM32_CORDIC_PHASE, x, y);
}

uint32_t n23_cordic_pythagoras(uint32_t x, uint32_t y){
    return binary_op(STM32_CORDIC_MODULUS, x, y);
}

uint32_t n23_cordic_atan(uint32_t x){
    return unary_op(STM32_CORDIC_ATAN,x);
}
uint32_t n23_cordic_atanh(uint32_t x){
    return unary_op(STM32_CORDIC_ATANH,x);
}

uint32_t n23_cordic_natural_log(uint32_t x){
    return unary_op(STM32_CORDIC_NATURAL_LOG,x);
}

uint32_t n23_cordic_sqrt(uint32_t x){
    return unary_op(STM32_CORDIC_SQRT,x);
}
