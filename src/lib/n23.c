#include "hw/flash.h"
#include "hw/gpio.h"
#include "hw/stm32g4.h"
#include <stdbool.h>
#include <stddef.h>
#ifdef N23_G431_BOARD_COMPAT
#define N23_POWER_LOW_LED (volatile stm32_gpio*)STM32_GPIOA, 10
#define N23_BACKLIGHT (volatile stm32_gpio*)STM32_GPIOA, 9
#else
#define N23_POWER_LOW_LED (volatile stm32_gpio*)STM32_GPIOC, 9
#define N23_BACKLIGHT (volatile stm32_gpio*)STM32_GPIOC, 6
#endif
void clock_core(uint32_t desired_clock_mhz)
{

    // Switch over to HSI for core clock while we modify the PLL
    stm32_rcc->config.system_clock = 1;
    while (stm32_rcc->config.system_clock_status != 1) { }

    // disable PLL so we can modify the config
    stm32_rcc->control.pll_enable = false;
    while (stm32_rcc->control.pll_ready) { }

    stm32_rcc_pll_config pll_config = stm32_rcc->pll_config;

    pll_config.source = 2; // Use 16MHz internal clock as input
    pll_config.input_division_factor = 3; // Each increment of the mulitplcation factor increases clock by 2Mhz (Actually 4 but PLL(PQR) has a min prescale of 2)
    uint32_t multiplication_factor = (desired_clock_mhz / 2) - 1;
    pll_config.vco_multiplication_factor = multiplication_factor;
    // PLLR is fed into the system clock so enable it
    pll_config.r_clock_enable = true;
    stm32_rcc->pll_config = pll_config;

    stm32_rcc->control.pll_enable = true;
    while (!stm32_rcc->control.pll_ready) { }

    // use PLL (through PLLR) rather than HSI for core clock (SYSCLK)
    stm32_rcc->config.system_clock = 3;
    while (stm32_rcc->config.system_clock_status != 3) { }

    // AHB/APB, and ADC are by default scaled off of SYSCLK so don't need to touch them.
    // TODO unless I'm overclocking...
}

void memory_latency(uint32_t desired_clock_mhz)
{
    //  set up FLASH latency
    uint32_t ws = (desired_clock_mhz / 34);
    if (ws >= 15) {
        ws = 15;
    }

    stm32_flash->access_control.latency = ws;
    while (stm32_flash->access_control.latency != ws) { };
}

uint32_t n23_get_core_clock_mhz(void)
{
    return (stm32_rcc->pll_config.vco_multiplication_factor + 1) * 2;
}

void n23_set_core_clock(uint32_t desired_clock_mhz)
{
    if (n23_get_core_clock_mhz() > desired_clock_mhz) {
        // Trying to clock down. In this case, declock the core, then
        // reduce the wait states to not bottleneck on ROM
        clock_core(desired_clock_mhz);
        memory_latency(desired_clock_mhz);
    } else {
        // Clocking up. Adjust wait states to keep up with clock, then clock up
        memory_latency(desired_clock_mhz);
        clock_core(desired_clock_mhz);
    }
}

void n23_set_up(uint32_t desired_clock_mhz)
{

    // be in Range 1 Boost mode voltage to get the best possible wait states and the highest possible clocks
    stm32_power->control_5.range_1_mode = 0;

    stm32_flash->access_control.prefetch = true;
    stm32_flash->access_control.instruction_cache_enable = true;
    stm32_flash->access_control.data_cache_enable = true;

    n23_set_core_clock(desired_clock_mhz);
#ifdef N23_G431_BOARD_COMPAT
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOA, true);
#else
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOC, true);
#endif
    stm32_gpio_mode_set(N23_POWER_LOW_LED, stm32_pin_mode_Output);
    stm32_gpio_mode_set(N23_BACKLIGHT, stm32_pin_mode_Output);

    // TODO start the battery indicator timer interrupt
}

void n23_power_led(bool val)
{
    stm32_gpio_out_set(N23_POWER_LOW_LED, val);
}

void n23_backlight(bool val)
{
    stm32_gpio_out_set(N23_BACKLIGHT, !val);
}
