
#include "lib/audio.h"
#include "hw/dac.h"
#include "hw/dma.h"
#include "hw/exti.h"
#include "hw/gpio.h"
#include "hw/stm32g4.h"
#include "hw/timers/basic.h"
#include "hw/timers/general.h"
#include "lib/n23.h"
#include <stdint.h>

#define AUDIO_DMA_CHANNEL 0

#define N23_AUDIO_RIGHT (volatile stm32_gpio*)STM32_GPIOA, 4
#define N23_AUDIO_LEFT (volatile stm32_gpio*)STM32_GPIOA, 5
#define N23_AUDIO_VOLUME (volatile stm32_gpio*)STM32_GPIOA, 3

#define N23_AUDIO_VOLU (volatile stm32_gpio*)STM32_GPIOC, 15
#define N23_AUDIO_VOLD (volatile stm32_gpio*)STM32_GPIOD, 2

#define N23_AUDIO_VOL_TIMER stm32_tim5

#define AUDIO_DAC stm32_dac1

#define AUDIO_SAMPLE_RATE_TIMER stm32_tim7

typedef struct {
    n23_audio_sample buffer[2][BUFFERED_SAMPLES];
    uint8_t buffer_index;
} n23_audio_buffers_data;

n23_audio_buffers_data n23_audio_buffers;
n23_audio_renderer n23_audio_current_renderer;

void set_up_init_signal_generators(void)
{
    // TODO calibrate the DAC
    stm32_clock_peripheral(stm32_peripheral_enable_DAC1, true);
    stm32_dac_control dac_config = {
        .enable_1 = true,
        .enable_2 = true,
        .trigger_enable_1 = true,
        .trigger_enable_2 = true,
        .trigger_select_1 = 0b0010, // Trigger DAC data request when TIM7 fires (48KHz)
        .trigger_select_2 = 0b0010,
        .waveform_1 = 0,
        .waveform_2 = 0,
        .waveform_amplitude_1 = 0,
        .waveform_amplitude_2 = 0,
        .dma_enable_1 = true,
        .dma_enable_2 = true,
        .dma_underrun_irq_enable_1 = false,
        .dma_underrun_irq_enable_2 = false,
        .dma_calibration_enable_1 = false,
        .dma_calibration_enable_2 = false,
        .__reserved_1 = 0,
        .__reserved_2 = 0,
    };
    AUDIO_DAC->control = dac_config;

    stm32_dac_mode_control dac_mode_control = { .mask = 0 };
    dac_mode_control.frequency = 0b10; // Compatible with fastest clock
    dac_mode_control.input_signed_1 = true;
    dac_mode_control.input_signed_2 = true;
    dac_mode_control.double_data_1 = false;
    dac_mode_control.double_data_2 = false;
    AUDIO_DAC->mode_control = dac_mode_control;

    stm32_gpio_mode_set(N23_AUDIO_RIGHT, stm32_pin_mode_Analog);
    stm32_gpio_mode_set(N23_AUDIO_LEFT, stm32_pin_mode_Analog);
}

void init_dma_transfer(uint32_t dma_channel_trigger)
{
    // Set up one side of the audio DMA to
    stm32_dma_device_config config = stm32_dma1->chan[AUDIO_DMA_CHANNEL].config;

    config.tcie = true;
    config.htie = true; // Get an interrupt every time half a channel has been sent
    config.teie = false; // Disable error interrupt
    config.dir = true; // Read from memory into peripheral
    config.circ = true; // Audio is a circular buffer
    config.pinc = false; // Point to the same memory location at all times
    config.minc = true; // Increment Memory address to read from
    config.psize = 0b10; // write into a 32 bit register
    config.msize =
#ifdef N23_AUDIO_8_BIT
        0b01;
#else
        0b10;
#endif
    config.pl = 0b11; // highest priority
    config.mem2mem = false;

    stm32_dma1->chan[AUDIO_DMA_CHANNEL].config = config;
    stm32_dma1->chan[AUDIO_DMA_CHANNEL].peripheral_address =
#ifdef N23_AUDIO_8_BIT
        // 8 bit audio
        (uint8_t*)&(AUDIO_DAC->data_holding[2].data_8);
#else
        // 16 bit audio
        (uint8_t*)&(AUDIO_DAC->data_holding[2].data_12l);
#endif

    stm32_dma1->chan[AUDIO_DMA_CHANNEL].memory_address = n23_audio_buffers.buffer;
    stm32_dma1->chan[AUDIO_DMA_CHANNEL].ndt = BUFFERED_SAMPLES * 2;

    // Trigger the DMA device every time the DAC requests a value (the DAC is triggered by the 48KHz clock)
    stm32_dmamux_request_channel_config cc = stm32_dmamux_device->cc[AUDIO_DMA_CHANNEL];
    cc.dmareq_id = dma_channel_trigger;
    stm32_dmamux_device->cc[AUDIO_DMA_CHANNEL] = cc;
}

void init_sample_clock(void)
{
    stm32_clock_peripheral(stm32_peripheral_enable_TIM7, true);
    stm32_timer_basic_config1 sample_rate_config = AUDIO_SAMPLE_RATE_TIMER->config1;
    sample_rate_config.update_disable = false;
    sample_rate_config.update_source = 0b1; // Generate an UPDATE event every time we overflow
    sample_rate_config.one_pulse_mode = false;
    sample_rate_config.auto_reload = true;
    sample_rate_config.uif_remap = false;
    sample_rate_config.dither = false; // Consider dithering since not all clock speeds can be divided into 48KHz neatly?
    AUDIO_SAMPLE_RATE_TIMER->config1 = sample_rate_config;

    // Allow the timer to generate update events to trigger the DMA
    AUDIO_SAMPLE_RATE_TIMER->config2.master_mode = 0b010;

    AUDIO_SAMPLE_RATE_TIMER->prescaler.prescaler = (n23_get_core_clock_mhz() * 1000) / 48; // Prescaled such that 1 tick takes 1/48000 of a second
    AUDIO_SAMPLE_RATE_TIMER->auto_reload.auto_reload = true;
}

uint8_t get_volume(void)
{
    return N23_AUDIO_VOL_TIMER->capture_compare[3];
}

void set_volume(uint8_t vol)
{
    N23_AUDIO_VOL_TIMER->capture_compare[3] = vol;
}

void n23_audio_init(void)
{

    stm32_clock_peripheral(stm32_peripheral_enable_GPIOA, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOC, true);
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOD, true);

    set_up_init_signal_generators();

    // Set up the volume pin
    // Set up PWM generating clock
    stm32_clock_peripheral(stm32_peripheral_enable_TIM5, true);
    stm32_timer_gp_long_config1 pwm_config = N23_AUDIO_VOL_TIMER->config1;
    pwm_config.update_disable = false;
    pwm_config.direction = 0b1; // Count Down. If we were counting up then update events would reset the prescaler (????)
    pwm_config.update_source = 0b1;
    pwm_config.one_pulse_mode = false;
    pwm_config.center_align_mode = 0b00; // Do not alternate directions
    pwm_config.auto_reload = true; // TODO: check we should not be buffering this, since CCR is *not* buffered (I think?)
    pwm_config.clock_division = 0b00;
    pwm_config.uif_remap = false;
    pwm_config.dither = false;
    N23_AUDIO_VOL_TIMER->config1 = pwm_config;
    N23_AUDIO_VOL_TIMER->prescaler.prescaler = 0;
    N23_AUDIO_VOL_TIMER->auto_reload = UINT8_MAX;
    N23_AUDIO_VOL_TIMER->capture_compare_enable.cc4_enable = true;
    N23_AUDIO_VOL_TIMER->capture_compare_mode[1].output_oc2m = 0b110;
    N23_AUDIO_VOL_TIMER->config1.enable = true;

    stm32_gpio_mode_set(N23_AUDIO_VOLUME, stm32_pin_mode_Alternate);
    stm32_gpio_speed_set(N23_AUDIO_VOLUME, stm32_pin_speed_VeryHigh);
    stm32_gpio_af_set(N23_AUDIO_VOLUME, 2); // TIM5CH4

    set_volume(0b00000000);

    // Set up the volume controls TODO: check that interrupt on D2 works. That
    // switch snapped off on the current devboard, and that line is always low
    // (after my resoldering job) so the interrupt never fires.
    cortex_m4_nvic_enable_interrupt(stm32_g4_interrupt_exti2);
    cortex_m4_nvic_enable_interrupt(stm32_g4_interrupt_exti_15_10);

    // enable the correct  pin to interrupt on
    stm32_clock_peripheral(stm32_peripheral_enable_SYSCFG, true);
    stm32_syscfg->exticr[0].exti2 = 0b0011; // D2
    stm32_syscfg->exticr[3].exti3 = 0b0010; // C15
    uint32_t exti_mask = (1 << 2) | (1 << 15);
    stm32_exti->interrupt_mask_1 = stm32_exti->interrupt_mask_1 | exti_mask;
    stm32_exti->falling_trigger_sel_1 = stm32_exti->falling_trigger_sel_1 | exti_mask;

    stm32_clock_peripheral(stm32_peripheral_enable_DMA1, true);
    stm32_clock_peripheral(stm32_peripheral_enable_DMA1MUX, true);
    // timer to trigger DMA requests (i.e signal level writes) at 48khz
    init_sample_clock();

    // Point DMA device at the DAC
    init_dma_transfer(STM32_DAC1_CH1);

    // TODO: should this be done in `start`, rather than `init`?
    // activate NVIC so we can get DMA1 ch1 interrupts when DMA has written out half the buffer
    cortex_m4_nvic_enable_interrupt(stm32_g4_interrupt_dma1_ch1);

    // Volume controls
    // Don't need to clock up EXTI, seems to be part of the core
    stm32_gpio_mode_set(N23_AUDIO_VOLU, stm32_pin_mode_Input);
    // stm32_gpio_speed_set(N23_AUDIO_VOLU, stm32_pin_speed_VeryHigh);
    stm32_gpio_pupd_set(N23_AUDIO_VOLU, stm32_pin_pull_Up);
    stm32_gpio_mode_set(N23_AUDIO_VOLD, stm32_pin_mode_Input);
    // stm32_gpio_speed_set(N23_AUDIO_VOLD, stm32_pin_speed_VeryHigh);
    stm32_gpio_pupd_set(N23_AUDIO_VOLD, stm32_pin_pull_Up);
}

void n23_audio_start(n23_audio_renderer initial_renderer)
{
    n23_audio_set_renderer(initial_renderer);
    //  Pre-fill the buffers
    initial_renderer(n23_audio_buffers.buffer[0]);
    initial_renderer(n23_audio_buffers.buffer[1]);
    n23_audio_buffers.buffer_index = 1;

    // signal generator (PWM/DAC) should start writing out

    stm32_dac_control dac_config = AUDIO_DAC->control;
    dac_config.enable_1 = true;
    dac_config.enable_2 = true;
    AUDIO_DAC->control = dac_config;
    // Start the DMAs

    stm32_dma_device_channel chan = stm32_dma1->chan[AUDIO_DMA_CHANNEL];
    chan.config.en = true;
    stm32_dma1->chan[AUDIO_DMA_CHANNEL] = chan;

    stm32_dma1->ifc = 0b1111 << (4 * AUDIO_DMA_CHANNEL); // Clear all interrupt flags for the DMA (Yes this could be nicer, idc for now)

    // Generate an update event to start the 48khz clock.
    AUDIO_SAMPLE_RATE_TIMER->generate_event.update = true;
    AUDIO_SAMPLE_RATE_TIMER->config1.enable = true;
}

void n23_audio_set_renderer(n23_audio_renderer new_renderer)
{
    n23_audio_current_renderer = new_renderer;
}

void arm_vector_irq_dma1_ch1(void)
{
    // Half of the audio buffers have been written out. Fill up that previous area
    // Only left DMA needs to have the interrups cleared, we assume right DMA is in the same position
    stm32_dma1->ifc = 0b1111 << (4 * AUDIO_DMA_CHANNEL); // Clear all interrupt flags for the DMA so we don't re-enter

    // swap buffers so we write to the just-emptied buffers
    if (n23_audio_buffers.buffer_index == 0) {
        n23_audio_buffers.buffer_index = 1;
    } else {
        n23_audio_buffers.buffer_index = 0;
    }

    n23_audio_current_renderer(n23_audio_buffers.buffer[n23_audio_buffers.buffer_index]);
}
// TODO  This one isn't firing, but the other is.
// TODO: figure out how to de-prioritise these ones, they probably shouldn't take precedence over, say, audio rendering
void arm_vector_irq_exti_2(void)
{
    // volume down
    uint8_t current_volume = get_volume();
    if (current_volume == 0) {
        current_volume = 0;
    } else {
        current_volume = current_volume >> 1;
    }
    set_volume(current_volume);
    // Clear the interrupt
    stm32_exti->pending_interupt_1 = stm32_exti->pending_interupt_1 & (0b1 << 2);
}

void arm_vector_irq_exti15_10(void)
// Check that it was on port C (i,e volume down button) TODO wont need to do that because of the way SYSCFgh works
{
    // volume up
    uint8_t current_volume = get_volume();
    if (current_volume == 0) {
        current_volume = 1;
    } else if (current_volume == 0b1 << 7) {
        current_volume = current_volume;
    } else {
        current_volume = current_volume << 1;
    }

    set_volume(current_volume);

    // Clear the interrupt
    stm32_exti->pending_interupt_1 = stm32_exti->pending_interupt_1 & (0b1 << 15);
}
