
#include <stddef.h>
#include <stdint.h>

#include "hw/gpio.h"
#include "hw/spi.h"
#include "hw/stm32g4.h"
#include "lib/n23.h"
#include "lib/timers.h"

#ifdef N23_G431_BOARD_COMPAT

#define STM32_DISPLAY_CS (volatile stm32_gpio*)STM32_GPIOA, 4
#define STM32_DISPLAY_DCS (volatile stm32_gpio*)STM32_GPIOA, 8
#define STM32_DISPLAY_RESET (volatile stm32_gpio*)STM32_GPIOB, 2
#define STM32_DISPLAY_CLK (volatile stm32_gpio*)STM32_GPIOB, 5
#define STM32_DISPLAY_MISO (volatile stm32_gpio*)STM32_GPIOB, 6
#define STM32_DISPLAY_MOSI (volatile stm32_gpio*)STM32_GPIOB, 7
#define STM32_DISPLAY_BUS stm32_spi1
#define STM32_DISPLAY_BUS_PERIPHERAL stm32_peripheral_enable_SPI1

#else

#define STM32_DISPLAY_CS (volatile stm32_gpio*)STM32_GPIOB, 12
#define STM32_DISPLAY_DCS (volatile stm32_gpio*)STM32_GPIOB, 11
#define STM32_DISPLAY_RESET (volatile stm32_gpio*)STM32_GPIOB, 10
#define STM32_DISPLAY_CLK (volatile stm32_gpio*)STM32_GPIOB, 13
#define STM32_DISPLAY_MISO (volatile stm32_gpio*)STM32_GPIOB, 14
#define STM32_DISPLAY_MOSI (volatile stm32_gpio*)STM32_GPIOB, 15
#define STM32_DISPLAY_BUS stm32_spi2
#define STM32_DISPLAY_BUS_PERIPHERAL stm32_peripheral_enable_SPI2

#endif

void send_command(uint8_t word)
{
    while (STM32_DISPLAY_BUS->s.bsy == 1) { }

    stm32_gpio_out_set(STM32_DISPLAY_DCS, false);

    // Commands are 8 bit, so move into 8b mode temporarily
    STM32_DISPLAY_BUS->c1.spe = 0;
    STM32_DISPLAY_BUS->c2.ds = 7;
    STM32_DISPLAY_BUS->c1.spe = 1;

    *(volatile uint8_t*)&(STM32_DISPLAY_BUS->d8) = word;

    while (STM32_DISPLAY_BUS->s.bsy == 1) { }

    // Move back to 16b mode now we're done sending the command
    STM32_DISPLAY_BUS->c1.spe = 0;
    STM32_DISPLAY_BUS->c2.ds = 15;
    STM32_DISPLAY_BUS->c1.spe = 1;
    stm32_gpio_out_set(STM32_DISPLAY_DCS, true);
}

void send_data_8b(uint8_t dat)
{
    while (STM32_DISPLAY_BUS->s.bsy == 1) { }

    // move into 8b mode temporarily
    STM32_DISPLAY_BUS->c1.spe = 0;
    STM32_DISPLAY_BUS->c2.ds = 7;
    STM32_DISPLAY_BUS->c1.spe = 1;

    *(volatile uint8_t*)&(STM32_DISPLAY_BUS->d8) = dat;

    while (STM32_DISPLAY_BUS->s.bsy == 1) { }

    // Move back to 16b mode now we're done sending the command
    STM32_DISPLAY_BUS->c1.spe = 0;
    STM32_DISPLAY_BUS->c2.ds = 15;
    STM32_DISPLAY_BUS->c1.spe = 1;
}

static inline uint16_t eswap16(uint16_t x) { return (x >> 8) | (x << 8); }

void display_select(bool select)
{
    while (STM32_DISPLAY_BUS->s.bsy == 1) { }
    stm32_gpio_out_set(STM32_DISPLAY_CS, !select);
}

union jank {
    volatile uint8_t* u8p;
    volatile uint16_t* u16p;
};

void send_data(uint16_t word)
{
    while (STM32_DISPLAY_BUS->s.txe == 0) { }
    *((volatile uint16_t*)&STM32_DISPLAY_BUS->d16) = word;
}

void display_send_column_addr(uint16_t min, uint16_t max)
{
    send_command(0x2A);
    uint16_t min_be = (min);
    send_data(min_be);
    uint16_t max_be = (max);
    send_data(max_be);
}

void display_send_page_addr(uint16_t min, uint16_t max)
{
    send_command(0x2B);
    uint16_t min_be = (min);
    send_data(min_be);
    uint16_t max_be = (max);
    send_data(max_be);
}

void display_send_pixel(uint16_t pix) { send_data(pix); }

void display_send_memory_write() { send_command(0x2C); }

void display_set_up(const uint32_t desired_clock_mhz)
{
#ifdef N23_G431_BOARD_COMPAT
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOA, true);
#endif
    stm32_clock_peripheral(stm32_peripheral_enable_GPIOB, true);

    // Pull NSS pin low to select the screen
    stm32_gpio_mode_set(STM32_DISPLAY_CS, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_DISPLAY_CS, stm32_pin_speed_VeryHigh);
    stm32_gpio_out_set(STM32_DISPLAY_CS, true);

    stm32_gpio_mode_set(STM32_DISPLAY_DCS, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_DISPLAY_DCS, stm32_pin_speed_VeryHigh);

    stm32_clock_peripheral(STM32_DISPLAY_BUS_PERIPHERAL, true);

    // Make the bus be controlled by SPI peripheral
    stm32_gpio_mode_set(STM32_DISPLAY_CLK, stm32_pin_mode_Alternate);
    stm32_gpio_af_set(STM32_DISPLAY_CLK, 5);
    stm32_gpio_speed_set(STM32_DISPLAY_CLK, stm32_pin_speed_VeryHigh);

    stm32_gpio_mode_set(STM32_DISPLAY_MISO, stm32_pin_mode_Alternate);
    stm32_gpio_af_set(STM32_DISPLAY_MISO, 5);
    stm32_gpio_speed_set(STM32_DISPLAY_MISO, stm32_pin_speed_VeryHigh);

    stm32_gpio_mode_set(STM32_DISPLAY_MOSI, stm32_pin_mode_Alternate);
    stm32_gpio_af_set(STM32_DISPLAY_MOSI, 5);
    stm32_gpio_speed_set(STM32_DISPLAY_MOSI, stm32_pin_speed_VeryHigh);

    // set up the SPI control registers
    stm32_spi_c2 c2 = { .mask = STM32_DISPLAY_BUS->c2.mask };
    c2.ds = 15; // 16 bits transfer words
    c2.frxth = 1;
    c2.ssoe = 1;

    (STM32_DISPLAY_BUS->c2) = c2;

    stm32_spi_c1 c1 = { .mask = STM32_DISPLAY_BUS->c1.mask };
    c1.ssm = 1;
    c1.ssi = 1;

    // get the clock speed we need
    uint32_t ratio_desired = n23_get_core_clock_mhz() / desired_clock_mhz;
    uint32_t ratio_actual = 2;
    uint8_t br_value = 0;
    while (ratio_actual < ratio_desired) {
        ratio_actual *= 2;
        br_value++;
    }
    c1.br = br_value;
    c1.mstr = 1;

    (STM32_DISPLAY_BUS->c1) = c1;

    c1.spe = 1;

    (STM32_DISPLAY_BUS->c1) = c1;

    // TODO move C registers to bitfields, set SPE last as if it is set before the
    // other they are not gauranteed ot have an effect

    // Do a hardware reset by pulling DISPLAY_RESET low then high
    stm32_gpio_mode_set(STM32_DISPLAY_RESET, stm32_pin_mode_Output);
    stm32_gpio_speed_set(STM32_DISPLAY_RESET, stm32_pin_speed_VeryHigh);
    stm32_gpio_out_set(STM32_DISPLAY_RESET, false);
    n23_timers_sleep(10000);

    stm32_gpio_out_set(STM32_DISPLAY_RESET, true);
    n23_timers_sleep(50000);
    n23_timers_sleep(50000);
    n23_timers_sleep(20000);
    display_select(true);

    // Reset
    send_command(0x01);

    // Boost power and clock to reach 119Hz
    // Cribbed from FBCP
    send_command(0xCB); /*Power Control A*/
    send_data_8b(0x39); /*Reserved*/
    send_data_8b(0x2C); /*Reserved*/
    send_data_8b(0x00); /*Reserved*/
    send_data_8b(0x34); /*REG_VD=1.6V*/
    send_data_8b(0x02); /*VBC=5.6V*/

    send_command(0xCF); /*Power Control B*/
    send_data_8b(0x00); /*Always Zero*/
    send_data_8b(0xC1); /*Power Control=0,DRV_ena=0,PCEQ=1*/
    send_data_8b(0x30); /*DC_ena=1*/ // Not sure what the effect is, set to default as per ILI9341 Application Notes v0.6 (2011/03/11) document (which is not apparently same as default at power on).

    send_command(0xE8); /*Driver Timing Control A*/
    send_data_8b(0x85);
    send_data_8b(0x00);
    send_data_8b(0x78); // Not sure what the effect is, set to default as per ILI9341 Application Notes v0.6 (2011/03/11) document (which is not apparently same as default at power on).

    send_command(0xEA); /*Driver Timing Control B*/
    send_data_8b(0x00);
    send_data_8b(0x00); // Not sure what the effect is, set to default as per ILI9341 Application Notes v0.6 (2011/03/11) document (which is not apparently same as default at power on).

    send_command(0xF7); /*Pump Ratio Control*/
    send_data_8b(0x20); // 0x20 is defaul, 0x00 "supersaturated", 0x30 recommended by FBCP

    send_command(0xC5); /*VCOM Control 1*/
    send_data_8b(0x3e); /*VCOMH=4.250V*/
    send_data_8b(0x28); /*VCOML=-1.500V*/ // Adjusting VCOM 1 and 2 can control display brightness

    send_command(0xC7); /*VCOM Control 2*/
    send_data_8b(0x86); /*VCOMH=VMH-58,VCOML=VML-58*/

    // MADCTL
    send_command(0x36);
    send_data_8b(0b11100000);

    // PIXFMT
    send_command(0x3A);
    send_data_8b(0b01010101);

    // 'Normal' display mode.
    send_command(0x13);

    // Max refresh rate
    // Cribbed from FBCP
    send_command(0xB1); /*Frame Rate Control (In Normal Mode/Full Colors)*/
    send_data_8b(0x00); /*DIVA=fosc*/
    send_data_8b(0x10); /*RTNA(Frame Rate)*/

    //    SPI_TRANSFE);R(0xB5/*Blanking Porch Control*/, 0x02/*VFP, vertical front porch*/, 0x02/*VBP, vertical back porch*/, 0x0A/*HFP, horizontal front porch*/, 0x14/*HBP, horizontal back porch*/); // These are the default values at power on
    send_command(0xB6); /*Display Function Control*/
    send_data_8b(0x08); /*PTG=Interval Scan,PT=V63/V0/VCOML/VCOMH*/
    send_data_8b(0x82); /*REV=1(Normally white),ISC(Scan Cycle)=5 frames*/
    send_data_8b(0x27); /*LCD Driver Lines=320*/

    send_command(0x11); /*Sleep Out*/

    // Exit sleep mode.
    n23_timers_sleep(50000);
    n23_timers_sleep(50000);
    n23_timers_sleep(50000);

    // Full colour mode
    send_command(0x38);

    // Display on.
    send_command(0x29);
    n23_timers_sleep(50000);
    n23_timers_sleep(50000);
    n23_timers_sleep(50000);

    display_select(false);
}
