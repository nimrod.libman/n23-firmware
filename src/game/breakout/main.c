

#include "game/breakout/sprites.h"
#include "lib/controls.h"
#include "lib/maths.h"
#include "lib/n23.h"
#include "lib/timers.h"

#define NUM_BALLS 8
#define BRICK_ROWS 20
#define BRICK_COLUMNS 18
#define N23_SPRITES_COUNT ((BRICK_ROWS * BRICK_COLUMNS) + NUM_BALLS + 1)

#include "lib/spriterenderer/lib.h"
#include <stdbool.h>
#include <stdint.h>

static inline uint16_t eswap16(uint16_t x) { return (x >> 8) | (x << 8); }

static inline uint32_t abs(int32_t x) {
  if (x > 0) {
    return x;
  } else {
    return -x;
  }
}

uint16_t min(uint16_t a, uint16_t b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}
uint16_t max(uint16_t a, uint16_t b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}

typedef union
{
  struct{
    uint16_t r : 5;
    uint16_t g: 6;
    uint16_t b: 5;
  };
  uint16_t mask;
} rgb565;


#define RGB(red, green, blue) ((rgb565){.r=(red>>3),.g=(green>>2),.b=(blue>>3)})


// Game state
uint32_t score = 0;

typedef struct {
  uint16_t pos_x;
  uint16_t pos_y;
  int8_t vel_x;
  int8_t vel_y;
} breakout_ball;

bool valid_ball(breakout_ball *ball) {
  return ball->vel_x != 0 || ball->vel_y != 0;
}

breakout_ball balls[NUM_BALLS];

uint16_t paddle_x = 160;
uint16_t paddle_y = 200;

// TODO powerups
typedef enum {
  breakout_powerup_TODO = 0,
} breakout_powerup;

typedef struct {
  uint8_t hits_left;
  breakout_powerup contained_powerup;
} breakout_brick;

#define PLAYFIELD_OFFSET_X ((DISPLAY_COLUMNS - BRICK_WIDTH * BRICK_COLUMNS) / 2)
#define PLAYFIELD_OFFSET_Y (DISPLAY_ROWS - (BRICK_HEIGHT * BRICK_ROWS))

breakout_brick bricks[BRICK_ROWS][BRICK_COLUMNS];

rgb565 greyscale_table[4] = {RGB(0, 0, 0), RGB(85, 85, 85),
                               RGB(170, 170, 170), RGB(255, 255, 255)};

rgb565 hit_colour_tables[8][4] = {
    {RGB(0, 0, 0), RGB(0, 0, 0), RGB(0, 0, 0), RGB(0, 0, 0)},
    {RGB(255, 0, 0), RGB(192, 0, 0), RGB(255, 32, 32), RGB(0, 0, 0)},
    {RGB(255, 192, 0), RGB(192, 128, 0), RGB(255, 192, 64), RGB(0, 0, 0)},
    {RGB(255, 255, 0), RGB(192, 192, 0), RGB(255, 255, 64), RGB(0, 0, 0)},
    {RGB(0, 255, 0), RGB(0, 192, 0), RGB(32, 255, 32), RGB(0, 0, 0)},
    {RGB(0, 0, 255), RGB(0, 0, 192), RGB(32, 32, 255), RGB(0, 0, 0)},
    {RGB(128, 0, 255), RGB(96, 0, 192), RGB(192, 32, 255), RGB(0, 0, 0)},
    {RGB(128, 0, 128), RGB(96, 0, 96), RGB(192, 32, 192), RGB(0, 0, 0)}};

void set_up_sprites(void) {
  for (int x = 0; x < BRICK_COLUMNS; x++) {
    for (int y = 0; y < BRICK_ROWS; y++) {
      sprites[y * BRICK_COLUMNS + x] = (n23_spriterenderer_sprite){
          .base = &null_spritebase,
          .x = PLAYFIELD_OFFSET_X + x * BRICK_WIDTH,
          .y = PLAYFIELD_OFFSET_Y + y * BRICK_HEIGHT,
          .colour_table = (uint16_t*)hit_colour_tables[0],
      };
    }
  }

  for (int i = 0; i < NUM_BALLS; i++) {
    sprites[BRICK_COLUMNS * BRICK_ROWS + i] = (n23_spriterenderer_sprite){
        .base = &null_spritebase,
        .x = 0,
        .y = 0,
        .colour_table = (uint16_t*)greyscale_table,
    };
  }

  sprites[BRICK_COLUMNS * BRICK_ROWS + NUM_BALLS] = (n23_spriterenderer_sprite){
      .base = &paddle_sprite,
      .x = 0,
      .y = 0,
      .colour_table = (uint16_t*)greyscale_table,
  };
}

void set_brick_hits(unsigned int x, unsigned int y, uint8_t hits) {
  bricks[y][x].hits_left = hits;
  sprites[y * BRICK_COLUMNS + x].colour_table = (uint16_t*)hit_colour_tables[hits];
  if (hits == 0) {
    sprites[y * BRICK_COLUMNS + x].base = &null_spritebase;
  } else {
    sprites[y * BRICK_COLUMNS + x].base = &brick_sprite;
  }
}

void set_ball_position(int index, uint16_t x, uint16_t y) {
  balls[index].pos_x = x;
  balls[index].pos_y = y;
  sprites[BRICK_COLUMNS * BRICK_ROWS + index].x = x - BALL_RADIUS;
  sprites[BRICK_COLUMNS * BRICK_ROWS + index].y = y - BALL_RADIUS;
}

void create_board() {
  breakout_brick empty = {.hits_left = 0,
                          .contained_powerup = breakout_powerup_TODO};
  for (int i = 0; i < BRICK_ROWS; i++) {
    for (int j = 0; j < BRICK_COLUMNS; j++) {
      bricks[i][j] = empty;
    }
  }
  for (int i = 1; i <= 7; i++) {
    for (int j = 1; j <= 16; j++) {
      if (j == 4 || j == 13) {
        continue;
      }
      set_brick_hits(j, i, 8 - i);
    }
  }
}

const uint16_t right_ball_bound = 320 - PLAYFIELD_OFFSET_X - BALL_RADIUS;
const uint16_t left_ball_bound = PLAYFIELD_OFFSET_X + BALL_RADIUS;

/// Move the ball one tick, check for any collisions with bricks or borders, and
/// if so, reflect the ball and damage the brick.
void process_ball(uint8_t ball_index) {

  breakout_ball *current_ball = &balls[ball_index];
  uint16_t current_ball_x = current_ball->pos_x;
  uint16_t current_ball_y = current_ball->pos_y;

  current_ball_x += current_ball->vel_x;
  current_ball_y += current_ball->vel_y;

  bool x_collision = true;
  bool y_collision = true;
  if (current_ball_x > right_ball_bound) {
    current_ball_x = right_ball_bound;
  } else if (current_ball_x < left_ball_bound) {
    current_ball_x = left_ball_bound;
  } else {
    x_collision = false;
  }

  if (current_ball_y < PLAYFIELD_OFFSET_Y + BALL_RADIUS) {
    current_ball_y = PLAYFIELD_OFFSET_Y + BALL_RADIUS;
  } else if (current_ball_y + BALL_RADIUS >= paddle_y &&
             paddle_x - PADDLE_WIDTH / 2 <= current_ball_x &&
             current_ball_x <= paddle_x + PADDLE_WIDTH / 2) {
    current_ball_y = paddle_y - BALL_RADIUS;
  } else if (current_ball_y + BALL_RADIUS >= paddle_y + PADDLE_HEIGHT) {
    // We have fallen to the end zone and the ball should be lost.
    // TODO delete the ball from the game!
    current_ball->vel_x = 0;
    current_ball->vel_y = 0;
  } else {
    y_collision = false;
  }

  uint8_t min_brick_row =
      (current_ball_y - BALL_RADIUS - PLAYFIELD_OFFSET_Y) / BRICK_HEIGHT;
  uint8_t max_brick_row =
      (current_ball_y + BALL_RADIUS - PLAYFIELD_OFFSET_Y) / BRICK_HEIGHT;

  uint8_t min_brick_col =
      (current_ball_x - BALL_RADIUS - PLAYFIELD_OFFSET_X) / BRICK_WIDTH;
  uint8_t max_brick_col =
      (current_ball_x + BALL_RADIUS - PLAYFIELD_OFFSET_X) / BRICK_WIDTH;

  for (int y = min_brick_row;
       y <= max_brick_row && !x_collision && !y_collision; y++) {

    uint16_t brick_top = PLAYFIELD_OFFSET_Y + y * BRICK_HEIGHT;
    uint16_t brick_bottom = brick_top + BRICK_HEIGHT;

    for (int x = min_brick_col;
         x <= max_brick_col && !x_collision && !y_collision; x++) {
      if (bricks[y][x].hits_left == 0) {
        continue;
      }

      uint16_t brick_left = PLAYFIELD_OFFSET_X + x * BRICK_WIDTH;
      uint16_t brick_right = brick_left + BRICK_WIDTH;

      if (current_ball_x >= brick_left && current_ball_x <= brick_right) {
        // Ball is horizontally aligned with this brick; eligible for
        // vertical bounce
        bool hit_from_above = (current_ball_y + BALL_RADIUS) < brick_bottom &&
                              (current_ball_y + BALL_RADIUS) > brick_top;
        bool hit_from_below = (current_ball_y - BALL_RADIUS) < brick_bottom &&
                              (current_ball_y - BALL_RADIUS) > brick_top;

        if (hit_from_above || hit_from_below) {
          if (hit_from_above) {
            current_ball_y = brick_top - BALL_RADIUS;
          } else {
            current_ball_y = brick_bottom + BALL_RADIUS;
          }
          y_collision = true;
          set_brick_hits(x, y, bricks[y][x].hits_left - 1);
          break;
        }
      }

      if (current_ball_y >= brick_top && current_ball_y <= brick_bottom) {
        // Ball is vertically aligned with this brick; eligible for
        // horizontal bounce
        int hit_from_left = ((current_ball_x + BALL_RADIUS) < brick_right &&
                             (current_ball_x + BALL_RADIUS) > brick_left);
        int hit_from_right = ((current_ball_x - BALL_RADIUS) < brick_right &&
                              (current_ball_x - BALL_RADIUS) > brick_left);

        if (hit_from_left || hit_from_right) {
          if (hit_from_left) {
            current_ball_x = brick_left - BALL_RADIUS;
          } else {
            current_ball_x = brick_right + BALL_RADIUS;
          }
          x_collision = true;
          set_brick_hits(x, y, bricks[y][x].hits_left - 1);
          break;
        }
      }

      // bouncing off of the corner.
      uint16_t corners[4][2] = {{brick_left, brick_bottom},
                                {brick_left, brick_top},
                                {brick_right, brick_bottom},
                                {brick_right, brick_top}};
      for (int i = 0; i < 4; i++) {
        int32_t x_offset = current_ball_x - corners[i][0];
        x_offset *= x_offset;
        int32_t y_offset = current_ball_y - corners[i][1];
        y_offset *= y_offset;

        if (x_offset + y_offset <= BALL_RADIUS * BALL_RADIUS) {
          x_collision = true;
          y_collision = true;
          set_brick_hits(x, y, bricks[y][x].hits_left - 1);
          break;
          // I would really like to do some trig here on angles of
          // reflection but it's not reasonable for small inteter values,
        }
      }
    }
  }

  if (x_collision) {
    current_ball->vel_x = -current_ball->vel_x;
  }
  if (y_collision) {
    current_ball->vel_y = -current_ball->vel_y;
  }

  set_ball_position(ball_index, current_ball_x, current_ball_y);
}

// TODO powerups and score
// TODO moving between levels
// TODO sound
void play_breakout() {
  const uint32_t screenspeed = 80;
  n23_set_up(160);
  n23_cordic_math_init();

  n23_timers_init();

  n23_input_buttons_init();
  n23_input_joy_init();

  n23_timers_start_ticker();

  n23_spriterenderer_init(screenspeed);
  n23_backlight(true);

  set_up_sprites();
  n23_spriterenderer_start();

  create_board();

  balls[0].vel_x = 2;
  balls[0].vel_y = -2;

  balls[0].pos_x = 160;
  balls[0].pos_y = 180;

  sprites[BRICK_COLUMNS * BRICK_ROWS + 0].base = &ball_sprite;

  while (true) {

    uint16_t half_paddle_width = PADDLE_WIDTH / 2;
    uint16_t right_paddle_bound = 320 - PLAYFIELD_OFFSET_X - half_paddle_width;
    uint16_t left_paddle_bound = PLAYFIELD_OFFSET_X + half_paddle_width;

    int8_t stick_pos = ((255-(n23_input_right_joy().x>>8)) >> 5) - 4;
    if (stick_pos == -4) {
      stick_pos = -3;
    }
    paddle_x += stick_pos;
    if (paddle_x > right_paddle_bound) {
      paddle_x = right_paddle_bound;
    } else if (paddle_x < left_paddle_bound) {
      paddle_x = left_paddle_bound;
    }

    sprites[BRICK_COLUMNS * BRICK_ROWS + NUM_BALLS].x =
        paddle_x - PADDLE_WIDTH / 2;
    sprites[BRICK_COLUMNS * BRICK_ROWS + NUM_BALLS].y =
        paddle_y;

    for (int b = 0; b < NUM_BALLS; b++) {
      breakout_ball *current_ball = &balls[b];
      if (!valid_ball(current_ball)) {
        continue;
      }

      process_ball(b);
    }

    n23_timers_sleep(10000);
  }
}
