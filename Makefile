NAME = n23-firmware

ELF_RELEASE = $(NAME).elf
ELF_DEBUG   = $(NAME)-debug.elf

HEX_RELEASE = $(NAME).hex
HEX_DEBUG   = $(NAME)-debug.hex

CROSS_COMPILE = arm-none-eabi-
TARGET_AS     = $(CROSS_COMPILE)as
TARGET_CC     = $(CROSS_COMPILE)gcc
TARGET_LD     = $(CROSS_COMPILE)gcc
TARGET_SIZE   = $(CROSS_COMPILE)size

INCLUDE          = -I include -I .build/include
FLAGS_ARCH       = -mlittle-endian -mcpu=cortex-m4
CFLAGS_COMMON    =  $(FLAGS_ARCH) $(INCLUDE) -MD -MP -Wall -Wextra -Werror -ffreestanding
CFLAGS_RELEASE   = -Os $(CFLAGS_COMMON) -DNDEBUG
CFLAGS_DEBUG     = -Og -g $(CFLAGS_COMMON)
LDFLAGS          = $(FLAGS_ARCH) -T link/stm32g431.ld -nostartfiles -nostdlib -nodefaultlibs -lgcc -Wl,--print-memory-usage

SRC         = $(shell find src -type f)
OBJ_RELEASE = $(patsubst src/%.c, .build/%.o, $(SRC))
DEP_RELEASE = $(patsubst src/%.c, .build/%.d, $(SRC))
OBJ_DEBUG   = $(patsubst src/%.c, .build/debug/%.o, $(SRC))
DEP_DEBUG   = $(patsubst src/%.c, .build/debug/%.d, $(SRC))
TOOL_SRC    = $(shell find tools -type f -name "*.c")
TOOL        = $(patsubst %.c, %, $(TOOL_SRC))

OPENOCD_CONFIG ?= link/n23.cfg


all: release

release: $(ELF_RELEASE)

debug: $(ELF_DEBUG)

# When I try to build the vector table (and therefore the whole thing), create the build directory structure
.build/vector_table.o: link/vector_table.s
	mkdir -p $(dir $@)
	$(TARGET_AS) $(FLAGS_ARCH) -o $@ $<

# When I try to create an object file, create a directory for it in `build`
.build/%.o: src/%.c
	mkdir -p $(dir $@)
	$(TARGET_CC) -c $(CFLAGS_RELEASE) -o $@ $<

# As above for debug builds?
.build/debug/%.o: src/%.c
	mkdir -p $(dir $@)
	$(TARGET_CC) -c $(CFLAGS_DEBUG) -o $@ $<



# When I want to build the final ELF, require the vector table and all the other C files to be built and invoke the linker
$(ELF_RELEASE): .build/vector_table.o $(OBJ_RELEASE)
	$(TARGET_LD) -o $@ $^ $(LDFLAGS)


# As above for debug builds
$(ELF_DEBUG): .build/vector_table.o $(OBJ_DEBUG)
	$(TARGET_LD) -o $@ $^ $(LDFLAGS)


# When I want a HEX file, require the ELF and hexify it.
$(HEX_RELEASE): $(ELF_RELEASE)
	objcopy -O ihex $^ $@

$(HEX_DEBUG): $(ELF_DEBUG)
	objcopy -O ihex $^ $@



flash-debug: $(ELF_DEBUG)
	openocd -f $(OPENOCD_CONFIG) -c "program $(ELF_DEBUG) verify reset exit"

flash: $(ELF_RELEASE)
	openocd -f $(OPENOCD_CONFIG) -c "program $(ELF_RELEASE) verify reset exit"

clean:
	rm -rf .build $(ELF_RELEASE) $(ELF_DEBUG) $(HEX_RELEASE) $(HEX_DEBUG)

gdb:
	gdb-multiarch $(ELF_DEBUG) -ex "target extended-remote | openocd -f $(OPENOCD_CONFIG) -c \"gdb_port pipe\""

-include $(DEP_RELEASE) $(DEP_DEBUG)

.PHONY : all autogen release debug flash clean gdb cppcheck size loc


